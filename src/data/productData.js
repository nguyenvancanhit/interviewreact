/**
 * Created by VanCanh on 3/11/2018.
 */
"use strict";

const productData =
    [
        {
            "id": "0",
            "level_1": 1,
            "level_2": 1,
            "sku": "5806323503793",
            "name": "Bộ Xiaomi Redmi 4X (16GB/2GB) + Miếng Dán Cường Lực + Ốp Lưng Silicon ",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nhung Mobile",
            "star": 3,
            "price": "2550000",
            "marketPrice": "3800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Thiết kế: Nguyên khối",
                "Màn hình: 5 inch, HD (720 x 1280 pixels)",
                "CPU: Snapdragon 435 8 nhân"
            ],
            "imgUrl": [
                "0.u5618.d20170918.t142522.496376_1.jpg",
                "1.u5618.d20170918.t142522.538758_1.jpg",
                "2.u5618.d20170918.t142522.577852_1.jpg"
            ],
            "imgAvatar": "0.u5618.d20170918.t142522.496376_1 (1).jpg"
        },
        {
            "id": "1",
            "level_1": 2,
            "level_2": 1,
            "sku": "8900357420495",
            "name": "Internet Tivi Sony 48 inch KDL- 48W650D - Hàng Nhập Khẩu",
            "trademark": "Sony",
            "commitmentName": "Huỳnh Như",
            "star": 4,
            "price": "10990000",
            "marketPrice": "16900000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại tivi: Internet Tivi cơ bản",
                "Màn hình: 48 inch",
                "Độ phân giải: Full HD (1920 x 1080)",
                "Kết nối: Wifi, HDMI, USB, LAN"
            ],
            "imgUrl": [
                "e79bb9b95561c8bced2a49c55ecaead6.png",
                "kdl-48w650d (1).u425.d20160404.t100714.jpg",
                "kdl-48w650d (2).u425.d20160404.t100714.jpg"
            ],
            "imgAvatar": "e79bb9b95561c8bced2a49c55ecaead6.png"
        },
        {
            "id": "2",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "3",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "4",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "5",
            "level_1": 6,
            "level_2": 1,
            "sku": "6001778229895",
            "name": "Máy Xay Sinh Tố Philips HR2115 (600W)",
            "trademark": "Panasonic",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "899000",
            "marketPrice": "1599000",
            "comment": "14",
            "residual": "20",
            "dateEnd": 5,
            "guarantee": "2 năm",
            "Advantages": [
                "Hoạt động mạnh mẽ với công suất 600W",
                "Dung tích cối: 1.5 Lít",
                "Chất liệu lưỡi dao: Thép không gỉ",
                "5 tốc độ xay linh hoạt"
            ],
            "imgUrl": [
                "may-xay-sinh-to-philips-hr2115-1_1.jpg",
                "6ed0da0d3c14305fc09ee55e016a174c.jpg",
                "c1b64392dbd067508b0f4606c889af6c.jpg"
            ],
            "imgAvatar": "may-xay-sinh-to-philips-hr2115-1_1 (1).jpg"
        },
        {
            "id": "6",
            "level_1": 7,
            "level_2": 1,
            "sku": "6001778229895",
            "name": "Súng Bắn Keo M-K (20W)",
            "trademark": "M-K",
            "commitmentName": "M-K",
            "star": 5,
            "price": "49000",
            "marketPrice": "299000",
            "comment": "40",
            "residual": "30",
            "dateEnd": 3,
            "guarantee": "3 năm",
            "Advantages": [
                "Hoạt động mạnh mẽ với công suất 20W",
                "Gọn, nhẹ, dễ sử dụng",
                "Tương đối bền",
                "Đáp ứng được các nhu cầu dân dụng"
            ],
            "imgUrl": [
                "1.u2409.d20170606.t180046.443275.jpg",
                "2.u2409.d20170606.t180046.487760.jpg",
                "z.u2409.d20170606.t180046.543869.jpg"
            ],
            "imgAvatar": "1.u2409.d20170606.t180046.443275.jpg"
        },
        {
            "id": "7",
            "level_1": 8,
            "level_2": 1,
            "sku": "3781205697819",
            "name": "Súng Bắn Keo M-K (20W)",
            "trademark": "Chú Tiểu",
            "commitmentName": "TADA FOODS",
            "star": 3,
            "price": "20000",
            "marketPrice": "28000",
            "comment": "10",
            "residual": "60",
            "dateEnd": 4,
            "guarantee": "1 năm",
            "Advantages": [
                "Rong Biển Sấy Chú Tiểu (50g)",
                "Gọn, nhẹ, dễ sử dụng",
                "Tương đối bền",
                "Đáp ứng được các nhu cầu dân dụng"
            ],
            "imgUrl": [
                "5-001.u2409.d20171102.t163007.576260.jpg",
                "5-002.u2409.d20171102.t163007.612689.jpg",
                "5-003.u2409.d20171102.t163007.644219.jpg"
            ],
            "imgAvatar": "5-001.u2409.d20171102.t163007.576260.jpg"
        },
        {
            "id": "8",
            "level_1": 9,
            "level_2": 1,
            "sku": "3781205697819",
            "name": "Đồ Chơi Gỗ Forkids Rút Gỗ Mộc Funny Tower",
            "trademark": "Forkids",
            "commitmentName": "Tiki Trading",
            "star": 2,
            "price": "99000 ",
            "marketPrice": "143000",
            "comment": "146",
            "residual": "40",
            "dateEnd": 10,
            "guarantee": "5 năm",
            "Advantages": [
                "Làm bằng gỗ cao cấp",
                "Bề mặt phủ sơn an toàn không độc hại",
                "Trò chơi tập thể thú vị",
                "Rèn luyện nhiều kỹ năng phát triển cho bé"
            ],
            "imgUrl": [
                "76d8e5548a4a1fb22196840b02cc9ab8.jpg",
                "8936085370085_1.u2442.d20160705.t122930.jpg",
                "8936085370085_2.u2442.d20160705.t122930.jpg"
            ],
            "imgAvatar": "76d8e5548a4a1fb22196840b02cc9ab8.jpg"
        },
        {
            "id": "9",
            "level_1": 10,
            "level_2": 1,
            "sku": "3781205697819",
            "name": "Xe Máy Honda SH Mode Phiên Bản Cá Tính 2018",
            "trademark": "Honda",
            "commitmentName": "Honda Sài Gòn",
            "star": 4,
            "price": "68000000 ",
            "marketPrice": " 69000000",
            "comment": "16",
            "residual": "40",
            "dateEnd": 30,
            "guarantee": "2 năm",
            "Advantages": [
                "Giá bán xe đã bao gồm thuế VAT, không bao gồm thuế trước bạ",
                "Hàng đã nhận không được đổi trả",
                "Khách hàng nhận xe và làm thủ tục giấy tờ tại đại lý chính hãng",
                "Thông tin bảo hành: 3 năm / 30.000 km"
            ],
            "imgUrl": [
                "330d0bf8f55fc4e869216f10588f452c.jpg",
                "d1ef0c56ebb07dce033163c888bbd5e3.jpg",
                "8936085370085_2.u2442.d20160705.t122930.jpg"
            ],
            "imgAvatar": "330d0bf8f55fc4e869216f10588f452c.jpg"
        },
        {
            "id": "10",
            "level_1": 11,
            "level_2": 1,
            "sku": "3781205697819",
            "name": "Harry Potter: A Pop-Up Book: Based On The Film Phenomenon",
            "trademark": "Lucy Kee Hardcover",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "492000 ",
            "marketPrice": " 821000",
            "comment": "18",
            "residual": "80",
            "dateEnd": 7,
            "guarantee": "5 năm",
            "Advantages": [
                "Sách vui và hấp dẫn",
                "Nhiều tình tiết li kì",
                "Tạo cảm giác trừu tượng",
                "Tăng khả năng tư duy"
            ],
            "imgUrl": [
                "330d0bf8f55fc4e869216f10588f452c.jpg",
                "1.u2751.d20170620.t174730.866888.jpg",
                "2.u2751.d20170620.t174730.899567.jpg"
            ],
            "imgAvatar": "7.u2751.d20170620.t174711.773766.jpg"
        },
        {
            "id": "11",
            "level_1": 1,
            "level_2": 2,
            "sku": "8304273944842",
            "name": "Điện Thoại Nokia 216 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": " 690000",
            "marketPrice": "820000",
            "comment": "18",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màn hình: TFT 2.4",
                "Camera: VGA",
                "Pin: 1020 mAh",
                "Số SIM: 2 SIM"
            ],
            "imgUrl": [
                "nokia-216den1.u504.d20161109.t181038.208221 (1).jpg",
                "nokia-216den2.u504.d20161109.t181038.243311.jpg",
                "nokia-216den3.u504.d20161109.t181038.272668.jpg"
            ],
            "imgAvatar": "nokia-216den1.u504.d20161109.t181038.208221.jpg"
        },
        {
            "id": "12",
            "level_1": 1,
            "level_2": 3,
            "sku": "1772938516630",
            "name": "Điện Thoại iPhone X 64GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Đăng Khoa",
            "star": 5,
            "price": " 25990000",
            "marketPrice": "29990000",
            "comment": "15",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mã Quốc Tế: LL/ZA/ZP/..",
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Màn hình: Super AMOLED capacitive touchscreen, 5.8 inch HD",
                "CPU: Apple A11 Bionic 6 nhân"
            ],
            "imgUrl": [
                "0c751f43222991a2f4d313f0785045a7 (1).jpg",
                "2.u4939.d20170918.t113145.543503_4_3_3.jpg",
                "3.u4939.d20170918.t113145.578791_4_3_3.jpg"
            ],
            "imgAvatar": "0c751f43222991a2f4d313f0785045a7.jpg"
        },
        {
            "id": "13",
            "level_1": 1,
            "level_2": 4,
            "sku": "4804032422627",
            "name": "iPad Wifi Cellular 32GB New 2017 - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Khoa Huy",
            "star": 5,
            "price": "10990000",
            "marketPrice": "12999000",
            "comment": "10",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: LED backlit LCD, 9.7 inch",
                "Camera Trước/Sau: 1.2MP/ 8MP"
            ],
            "imgUrl": [
                "1.u2769.d20170624.t083902.626464 (1).jpg",
                "2.u2769.d20170624.t083902.682732.jpg",
                "3.u2769.d20170624.t083902.719222.jpg"
            ],
            "imgAvatar": "1.u2769.d20170624.t083902.626464.jpg"
        },
        {
            "id": "14",
            "level_1": 1,
            "level_2": 5,
            "sku": "1603222178442",
            "name": "Điện Thoại Samsung Galaxy A8 (2018) - Hàng Chính Hãng",
            "trademark": "Samsung",
            "commitmentName": "Huy Đăng",
            "star": 5,
            "price": "9990000",
            "marketPrice": "10990000",
            "comment": "18",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế viền cong",
                "Camera Trước/Sau: 16MP và 8MP/16MP"
            ],
            "imgUrl": [
                "8e52fa01289b1dd7527ace4d29cf83d6.jpg",
                "09398d178bb02554af871ccf9ca56628.jpg",
                "72242d19aa8f5bc819a98ddd07425185.jpg"
            ],
            "imgAvatar": "36417a530a0adced3fcf39d7892f1a3a.jpg"
        },
        {
            "id": "15",
            "level_1": 1,
            "level_2": 6,
            "sku": "5803257828411",
            "name": "Điện Thoại Xiaomi Redmi Note 4 (64GB/4GB) - Hàng Chính Hãng DGW",
            "trademark": "Xiaomi",
            "commitmentName": "Huy Võ",
            "star": 5,
            "price": "3890000",
            "marketPrice": "5990000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "ROM Tiếng Việt, có sẵn CH play",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 5.5 inch, Full HD (1080 x 1920 pixels)",
                "Camera Trước/Sau: 5MP/ 13MP"
            ],
            "imgUrl": [
                "mph1075h_1.u2769.d20170708.t101837.551785 (2).jpg",
                "mph1075h_2.u2769.d20170708.t101837.608510.jpg",
                "mph1560h.u2769.d20170708.t101837.649085.jpg"
            ],
            "imgAvatar": "mph1075h_1.u2769.d20170708.t101837.551785 (1).jpg"
        },
        {
            "id": "16",
            "level_1": 1,
            "level_2": 6,
            "sku": "5800843299983",
            "name": "Điện Thoại Xiaomi Redmi Note 4 (64GB/3GB) - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "MOBILEGIASI",
            "star": 5,
            "price": "3975000",
            "marketPrice": "5990000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: Cảm ứng điện dung IPS LCD, 16 triệu màu",
                "Tính năng: Cảm biến vân tay một chạm"
            ],
            "imgUrl": [
                "1.u5488.d20170725.t170900.1841 (2).jpg",
                "2.u5488.d20170725.t170900.44205.jpg"
            ],
            "imgAvatar": "1.u5488.d20170725.t170900.1841 (1).jpg"
        },
        {
            "id": "17",
            "level_1": 1,
            "level_2": 6,
            "sku": "2896405911069",
            "name": "Điện Thoại Xiaomi Redmi Note 5A - Hàng Chính Hãng DGW",
            "trademark": "Xiaomi",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "3290000",
            "marketPrice": "3490000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Kim loại nguyên khối",
                "CPU Qualcomm Snapdragon 425 4 nhân 64-bit"
            ],
            "imgUrl": [
                "fb79800e2cf80336101166b2866ee5d2 (2).jpg",
                "9d5b480fdcac781c1a16e97d2cb0603f.jpg",
                "0733d32bcff8b84e66c8b0c9a8027d50.jpg"
            ],
            "imgAvatar": "fb79800e2cf80336101166b2866ee5d2 (1).jpg"
        },
        {
            "id": "18",
            "level_1": 1,
            "level_2": 6,
            "sku": "5808545243839",
            "name": "Điện Thoại Xiaomi Redmi Note 5A (32GB/3GB) - Đen - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nhung Mobile",
            "star": 4,
            "price": "3090000",
            "marketPrice": "4500000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 5.5 inch Full HD",
                "Camera Trước/Sau: 5MP / 13MP"
            ],
            "imgUrl": [
                "1.u5552.d20170831.t092148.62548 (1).jpg",
                "2.u5552.d20170831.t092148.104995 (1).jpg"
            ],
            "imgAvatar": "1.u5552.d20170831.t092148.62548.jpg"
        },
        {
            "id": "19",
            "level_1": 1,
            "level_2": 7,
            "sku": "5808545243839",
            "name": "Điện Thoại Sony Xperia XZ Premium - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Khang Hoàng",
            "star": 4,
            "price": "14790000",
            "marketPrice": "18490000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa active",
                "Thiết kế: Nguyên khối",
                "Camera Trước/Sau: 19MP/13MP",
                "Nano SIM & Micro SIM (SIM 2 chung khe thẻ nhớ)"
            ],
            "imgUrl": [
                "625f626302c5969765b92698849a74a2 (2).jpg",
                "df797c30a6e2b8971f7c9e1b09f2e222.jpg",
                "40ed0ed8522397e6a184a7c691963e1d.jpg"
            ],
            "imgAvatar": "625f626302c5969765b92698849a74a2 (1).jpg"
        },
        {
            "id": "20",
            "level_1": 1,
            "level_2": 8,
            "sku": "5806960111436",
            "name": "Điện Thoại HTC 10 Evo - Hàng Chính Hãng",
            "trademark": "HTC",
            "commitmentName": "Khang Hoàng",
            "star": 4,
            "price": "4290000",
            "marketPrice": "5990000",
            "comment": "5",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100% ",
                "Màn hình: 5.5 inch, độ phân giải 2K (1440 x 2560 pixels)",
                "Camera Trước/Sau: 8MP/ 16MP",
                "CPU: Snapdragon 810 8 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u2769.d20170623.t174134.307519 (2).jpg",
                "2.u2769.d20170623.t174134.351263.jpg",
                "3.u2769.d20170623.t174134.390488.jpg"
            ],
            "imgAvatar": "1.u2769.d20170623.t174134.307519 (2).jpg"
        },
        {
            "id": "21",
            "level_1": 1,
            "level_2": 9,
            "sku": "5806960111436",
            "name": "Điện Thoại Huawei GR5 2017 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Nguyễn Khang",
            "star": 5,
            "price": "5990000",
            "marketPrice": "6990000",
            "comment": "15",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "amera Trước/Sau: 8MP/12MP và 2MP"
            ],
            "imgUrl": [
                "huawei-gr5-2017-den-1.u504.d20161226.t115102.822059 (1).jpg",
                "huawei-gr5-2017-1.u504.d20161226.t100341.859804.jpg",
                "huawei-gr5-2017-2.u504.d20161226.t100341.913211.jpg"
            ],
            "imgAvatar": "huawei-gr5-2017-den-1.u504.d20161226.t115102.822059.jpg"
        },
        {
            "id": "22",
            "level_1": 1,
            "level_2": 10,
            "sku": "8323151024216",
            "name": "Điện Thoại Samsung Galaxy Note 8 - Hàng Chính Hãng",
            "trademark": "Samsung",
            "commitmentName": "Nguyễn Khang",
            "star": 5,
            "price": "19950000",
            "marketPrice": "21350000",
            "comment": "15",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, màn hình vô cực không viền",
                "Màn hình: 6.3 inches (1440 x 2960 pixels)"
            ],
            "imgUrl": [
                "1.u3059.d20170825.t162110.714084_3 (6).jpg",
                "3.u3059.d20170825.t162110.756079_3.jpg",
                "5.u4939.d20170825.t164834.921718.jpg"
            ],
            "imgAvatar": "1.u3059.d20170825.t162110.714084_3.jpg"
        },
        {
            "id": "23",
            "level_1": 1,
            "level_2": 11,
            "sku": "4809366888354",
            "name": "iPad WiFi 32GB New 2017 - Hàng Chính Hãng",
            "trademark": " Apple",
            "commitmentName": "Nguyễn Hoàng",
            "star": 4,
            "price": "7750000",
            "marketPrice": "8999000",
            "comment": "15",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: LED backlit LCD, 9.7 inch",
                "CPU: Apple A9 2 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u2769.d20170623.t134206.993432 (1).jpg",
                "2.u2769.d20170623.t134207.53662.jpg",
                "3.u2769.d20170623.t134207.100790.jpg"
            ],
            "imgAvatar": "1.u2769.d20170623.t134206.993432.jpg"
        },
        {
            "id": "24",
            "level_1": 1,
            "level_2": 11,
            "sku": "4809366888354",
            "name": "Điện Thoại iPhone X 256GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "29400000",
            "marketPrice": "34790000",
            "comment": "10",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mã Quốc Tế: LL/ZA/ZP/..",
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Camera Trước/Sau: 7MP/ 2 camera 12MP"
            ],
            "imgUrl": [
                "a0f3f0400838ac900b1fa1bd48e4f636 (1).jpg",
                "2.u4939.d20170918.t113145.543503_3_3_3_3.jpg",
                "3.u4939.d20170918.t113145.578791_3_3_3_3.jpg"
            ],
            "imgAvatar": "a0f3f0400838ac900b1fa1bd48e4f636.jpg"
        },
        {
            "id": "25",
            "level_1": 1,
            "level_2": 11,
            "sku": "5805871271802",
            "name": "Điện Thoại iPhone 6s 32GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "12100000",
            "marketPrice": "12990000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Màn hình: 4.7 inch, HD (1334 x 750 Pixels)"
            ],
            "imgUrl": [
                "32gbgray_1.u504.d20161031.t112302.986490 (1).jpg",
                "32gbgray_2.u504.d20161031.t112303.34907.jpg",
                "32gbgray_3.u504.d20161031.t112303.75300.jpg"
            ],
            "imgAvatar": "32gbgray_1.u504.d20161031.t112302.986490.jpg"
        },
        {
            "id": "26",
            "level_1": 1,
            "level_2": 11,
            "sku": "5801713328154",
            "name": "Điện Thoại iPhone 6s Plus 32GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "iStore",
            "star": 4,
            "price": "13500000",
            "marketPrice": "14990000",
            "comment": "20",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Camera Trước/Sau: 5MP/ 12MP"
            ],
            "imgUrl": [
                "iphone-6s-plus-silver-1.u504.d20161031.t111819.627055 (1).jpg",
                "iphone-6s-plus-silver-2.u504.d20161031.t111819.664957.jpg",
                "iphone-6s-plus-silver-3.u504.d20161031.t111819.690656.jpg"
            ],
            "imgAvatar": "iphone-6s-plus-silver-1.u504.d20161031.t111819.627055.jpg"
        },
        {
            "id": "27",
            "level_1": 1,
            "level_2": 12,
            "sku": "5807793332166",
            "name": "Điện Thoại Nokia 8 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "8190000",
            "marketPrice": "12990000",
            "comment": "10",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Thiết kế: Nguyên khối",
                "Màn hình: IPS LCD, 2K (1440 x 2560 pixels)",
                "CPU: Qualcomm Snapdragon 835 8 nhân 64-bit"
            ],
            "imgUrl": [
                "ee379ccf2c5ff96434f8350d22ac0738 (1).jpg",
                "1.u4939.d20170729.t150616.198478.jpg",
                "2.u4939.d20170729.t150616.247357.jpg"
            ],
            "imgAvatar": "ee379ccf2c5ff96434f8350d22ac0738.jpg"
        },
        {
            "id": "28",
            "level_1": 1,
            "level_2": 12,
            "sku": "5807793332166",
            "name": "Điện Thoại Nokia 6 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4790000",
            "marketPrice": "5590000",
            "comment": "65",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Thiết kế: Nguyên khối",
                "Màn hình: 5.5 inch, Full HD (1080 x 1920 pixels)",
                "Camera Trước/Sau: 8MP/ 16MP"
            ],
            "imgUrl": [
                "c5092df6ebbc4660cce6c596ea3e9a4d (1).jpg",
                "1257e8d642cd33155df6efc0deb0345c.jpg",
                "e94cd95525a14ca613d6851322783f04.jpg"
            ],
            "imgAvatar": "c5092df6ebbc4660cce6c596ea3e9a4d.jpg"
        },
        {
            "id": "29",
            "level_1": 1,
            "level_2": 12,
            "sku": "5807793332166",
            "name": "Điện Thoại Nokia 5 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki",
            "star": 4,
            "price": "3690000",
            "marketPrice": "4260000",
            "comment": "65",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Thiết kế: Nguyên khối",
                "Màn hình: IPS LCD (720 x 1280 pixels)",
                "CPU: Qualcomm Snapdragon 430 8 nhân 64 bit"
            ],
            "imgUrl": [
                "1.u2769.d20170629.t121053.896161.jpg",
                "2.u2769.d20170629.t121053.946599.jpg",
                "3.u2769.d20170629.t121053.988924.jpg"
            ],
            "imgAvatar": "1.u2769.d20170629.t121053.896161.jpg"
        },
        {
            "id": "30",
            "level_1": 1,
            "level_2": 12,
            "sku": "5807793332166",
            "name": "Điện Thoại Nokia 2 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Khang",
            "star": 4,
            "price": "2070000",
            "marketPrice": "2400000",
            "comment": "45",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Thiết kế nguyên khối kim loại",
                "Màn hình 5 inch LTPS LCD HD (720 x 1280 pixels)",
                "Camera Trước/Sau: 5MP/8MP"
            ],
            "imgUrl": [
                "54d35c86da5a16072a8c423566c880b7.jpg",
                "97621c53555a552ffdb6a016747030ed.jpg",
                "210fd4a8fafeb402de7c48bdbf482a79.jpg"
            ],
            "imgAvatar": "54d35c86da5a16072a8c423566c880b7.jpg"
        },
        {
            "id": "31",
            "level_1": 1,
            "level_2": 13,
            "sku": "5806323503793",
            "name": "Bộ Xiaomi Redmi 4X (16GB/2GB) + Miếng Dán Cường Lực + Ốp Lưng Silicon - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nhung Mobile",
            "star": 3,
            "price": "2550000",
            "marketPrice": "3800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Thiết kế: Nguyên khối",
                "Màn hình: 5 inch, HD (720 x 1280 pixels)",
                "CPU: Snapdragon 435 8 nhân"
            ],
            "imgUrl": [
                "0.u5618.d20170918.t142522.496376_1.jpg",
                "1.u5618.d20170918.t142522.538758_1.jpg",
                "2.u5618.d20170918.t142522.577852_1.jpg"
            ],
            "imgAvatar": "0.u5618.d20170918.t142522.496376_1 (1).jpg"
        },
        {
            "id": "32",
            "level_1": 1,
            "level_2": 13,
            "sku": "5806035425765",
            "name": "Bộ Xiaomi Note 4X (16GB/3GB) + Miếng Dán Cường Lực + Ốp Lưng Silicon - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nhung Mobile",
            "star": 4,
            "price": "2899000",
            "marketPrice": "4500000",
            "comment": "4",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Thiết kế: Nguyên khối",
                "Miễn phí giao hàng toàn quốc",
                "Bộ Nhớ: 16GB"
            ],
            "imgUrl": [
                "0.0.u5618.d20170918.t154238.396014 (1).jpg",
                "1.u5618.d20170918.t154238.442176.jpg",
                "2.u5618.d20170918.t154238.468137.jpg"
            ],
            "imgAvatar": "0.u5618.d20170918.t154238.396014.jpg"
        },
        {
            "id": "33",
            "level_1": 1,
            "level_2": 13,
            "sku": "7608652452203",
            "name": "Điện Thoại Xiaomi Redmi 5A (16GB/2GB) - Hàng Chính Hãng",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nguyễn",
            "star": 4,
            "price": "1950000",
            "marketPrice": "2290000",
            "comment": "4",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%",
                "CPU: Qualcomm Snapdragon 425 4 nhân 64-bit 1.4 GHz",
                "Bộ Nhớ: 16 GB",
                "SIM tương thích: Nano SIM & Micro SIM (SIM 2 chung khe thẻ nhớ)"
            ],
            "imgUrl": [
                "5c5da64eddb36fe282e9a60e850e8a50.jpg",
                "5fa462bd50686cf15cfd70eaed32482b.jpg",
                "23ec98e02fc4b349da5bb1ac6157d888.jpg"
            ],
            "imgAvatar": "5c5da64eddb36fe282e9a60e850e8a50.jpg"
        },
        {
            "id": "34",
            "level_1": 1,
            "level_2": 13,
            "sku": "4895053312202",
            "name": "Điện Thoại Xiaomi Redmi 5 Plus (32GB/3GB) - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nguyễn",
            "star": 4,
            "price": "3880000 ",
            "marketPrice": "3890.00",
            "comment": "4",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: IPS LCD, 5.99",
                "Camera Trước/Sau: 5MP và 12MP"
            ],
            "imgUrl": [
                "01c2553d9902d835be652cf49b32e811.jpg",
                "35009c04d83467257e304d8d56e66c16.jpg",
                "7d849a354050829d53301c1cefb06390.jpg"
            ],
            "imgAvatar": "01c2553d9902d835be652cf49b32e811.jpg"
        },
        {
            "id": "35",
            "level_1": 1,
            "level_2": 14,
            "sku": "5801607325290",
            "name": "Điện Thoại OPPO F1s (32GB/3GB) - Hàng Chính Hãng",
            "trademark": " OPPO",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3880000 ",
            "marketPrice": "3890000",
            "comment": "4",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "Chính hãng, nguyên seal, mới 100%",
                "Màn hình: IPS LCD 5.5 HD (2.5D,Gorilla Glass 4)",
                "Camera Trước/Sau: 16MP / 13MP"
            ],
            "imgUrl": [
                "1.u2769.d20170503.t183534.986662.jpg",
                "2.u2769.d20170503.t183535.25392.jpg",
                "3.u2769.d20170503.t183535.55058.jpg"
            ],
            "imgAvatar": "1.u2769.d20170503.t183534.986662.jpg"
        },
        {
            "id": "36",
            "level_1": 1,
            "level_2": 14,
            "sku": "5801607325290",
            "name": "Điện Thoại OPPO F5 - Hàng Chính Hãng",
            "trademark": " OPPO",
            "commitmentName": "Trung Hiếu",
            "star": 4,
            "price": "6990000 ",
            "marketPrice": "7990000 ",
            "comment": "4",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "hính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Màn hình 6 inch",
                "ROM/RAM: 32GB/4GB"
            ],
            "imgUrl": [
                "00.u5552.d20171101.t161932.633377.jpg",
                "0.u5552.d20171101.t161932.682766.jpg",
                "2.u5552.d20171101.t161932.736797.jpg"
            ],
            "imgAvatar": "00.u5552.d20171101.t161932.633377.jpg"
        },
        {
            "id": "37",
            "level_1": 1,
            "level_2": 14,
            "sku": "807483066296",
            "name": "Điện Thoại OPPO F3 - Hàng Chính Hãng",
            "trademark": " OPPO",
            "commitmentName": "Trung Hiếu",
            "star": 4,
            "price": "4990000",
            "marketPrice": "6990000",
            "comment": "4",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "hính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Camera Trước/Sau: 16MP và 8MP/13MP"
            ],
            "imgUrl": [
                "1.u5395.d20170803.t122400.533902.jpg",
                "2.u5395.d20170803.t122400.570490.jpg",
                "3.u5395.d20170803.t122400.604363.jpg"
            ],
            "imgAvatar": "1.u5395.d20170803.t122400.533902.jpg"
        },
        {
            "id": "38",
            "level_1": 1,
            "level_2": 14,
            "sku": "6031677811044",
            "name": "Điện Thoại OPPO A83 (32GB/3GB) - Hàng Chính Hãng",
            "trademark": " OPPO",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4990000",
            "marketPrice": "6990000",
            "comment": "4",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "hính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Màn hình: IPS LCD, 5.7",
                "CPU: Mediatek Helio P23"
            ],
            "imgUrl": [
                "9df49d9dc4f77a3be390bfebe73db394.jpg",
                "ad9a195b4f2e473a055828523fcf9ca8.jpg",
                "a5a536f1ef546a023e2259316a5f5b93.jpg"
            ],
            "imgAvatar": "9df49d9dc4f77a3be390bfebe73db394.jpg"
        },
        {
            "id": "39",
            "level_1": 1,
            "level_2": 15,
            "sku": "5801995350140",
            "name": "Điện Thoại Sony XA1 Plus (32GB/4GB) - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Nguyễn Vân",
            "star": 4,
            "price": "5490000",
            "marketPrice": "7190000",
            "comment": "4",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "hính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Màn hình 5.5 inch FullHD",
                "Camera Trước/Sau: 8MP/23MP"
            ],
            "imgUrl": [
                "1.u5552.d20170918.t190303.348982.jpg",
                "3.u5552.d20170918.t190303.392976.jpg",
                "3_1.u5552.d20170918.t190303.435878.jpg"
            ],
            "imgAvatar": "1.u5552.d20170918.t190303.348982.jpg"
        },
        {
            "id": "40",
            "level_1": 1,
            "level_2": 15,
            "sku": "5801336457316",
            "name": "Điện Thoại Sony Xperia XA F3116 - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Nguyễn Vân",
            "star": 4,
            "price": " 3190000",
            "marketPrice": "4890000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nhựa nguyên khối(mặt kính cong 2,5D)",
                "Màn hình: IPS LCD, 5 inch HD",
                "Camera Trước/Sau: 8MP/13MP"
            ],
            "imgUrl": [
                "xperiaxarose1.u2470.d20160602.t105708.jpg",
                "xperiaxarose2.u2470.d20160602.t105708.jpg",
                "xperiaxarose3.u2470.d20160602.t105709.jpg"
            ],
            "imgAvatar": "xperiaxarose1.u2470.d20160602.t105708.jpg"
        },
        {
            "id": "41",
            "level_1": 1,
            "level_2": 15,
            "sku": "5808545243839",
            "name": "Điện Thoại Sony Xperia XZ Premium - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Khang Hoàng",
            "star": 4,
            "price": "14790000",
            "marketPrice": "18490000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa active",
                "Thiết kế: Nguyên khối",
                "Camera Trước/Sau: 19MP/13MP",
                "Nano SIM & Micro SIM (SIM 2 chung khe thẻ nhớ)"
            ],
            "imgUrl": [
                "625f626302c5969765b92698849a74a2 (2).jpg",
                "df797c30a6e2b8971f7c9e1b09f2e222.jpg",
                "40ed0ed8522397e6a184a7c691963e1d.jpg"
            ],
            "imgAvatar": "625f626302c5969765b92698849a74a2 (1).jpg"
        },
        {
            "id": "42",
            "level_1": 1,
            "level_2": 15,
            "sku": "4757839507052",
            "name": "Điện Thoại Sony Xperia L2 H4331 - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Mỹ Lệ",
            "star": 4,
            "price": "4890000",
            "marketPrice": "5490000",
            "comment": "12",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa active",
                "Thiết kế: Nguyên khối",
                "Camera trước/sau: 8MP/13MP",
                "CPU: MT6737T, 4 nhân)"
            ],
            "imgUrl": [
                "447ef08c36571a6b14f1424e949f05a6.jpg",
                "0c598249ed912a68dfc9f5f7757d94a1.jpg",
                "5a3ccda46c61689704992afd26146e87.jpg"
            ],
            "imgAvatar": "447ef08c36571a6b14f1424e949f05a6.jpg"
        },
        {
            "id": "43",
            "level_1": 1,
            "level_2": 16,
            "sku": "4757839507052",
            "name": "Điện Thoại Huawei Nova 2i - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Mỹ Duyên",
            "star": 4,
            "price": "5390000",
            "marketPrice": "5990000",
            "comment": "12",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa active",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: Full HD (1080 x 2160 pixels)",
                "Camera Trước/Sau: 13 MP và 2 MP / 16 MP và 2 MP (2 camera)"
            ],
            "imgUrl": [
                "7c4f2ccca031dc45369c582efe655c08.jpg",
                "4.u4939.d20171018.t145213.979379.jpg",
                "5.u4939.d20171018.t145214.18544.jpg"
            ],
            "imgAvatar": "7c4f2ccca031dc45369c582efe655c08.jpg"
        },
        {
            "id": "44",
            "level_1": 1,
            "level_2": 16,
            "sku": "5800987766099",
            "name": "Điện Thoại Huawei Y3 2017 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Mỹ Duyên",
            "star": 4,
            "price": "1890000 ",
            "marketPrice": "2190000",
            "comment": "20",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "thiết kế: Khung kim loại, nắp nhựa",
                "Miễn phí giao hàng toàn quốc",
                "CPU: MediaTek MT6580M 4 nhân Cortex-A7 1.3 GHz",
                "SIM tương thích: Micro SIM"
            ],
            "imgUrl": [
                "1.u5367.d20170711.t153316.2355_1.jpg",
                "2.u5367.d20170711.t153316.44926_1.jpg",
                "4.u5367.d20170711.t153316.101396_1.jpg"
            ],
            "imgAvatar": "1.u5367.d20170711.t153316.2355_1.jpg"
        },
        {
            "id": "45",
            "level_1": 1,
            "level_2": 16,
            "sku": "5802433047776",
            "name": "Điện Thoại Huawei Y5 2017 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Viễn Tin Sài Gòn",
            "star": 5,
            "price": "2589000 ",
            "marketPrice": "2990000",
            "comment": "20",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "thiết kế: Khung kim loại, nắp nhựa",
                "Miễn phí giao hàng toàn quốc",
                "Camera Trước: 5 MP, flash LED",
                "CPU: 4 nhân Cortex-A53 1.4 GHz"
            ],
            "imgUrl": [
                "1.u5367.d20170630.t111628.609739_2.jpg",
                "2.u5367.d20170630.t111628.663014_2.jpg",
                "3.u5367.d20170630.t111628.704850_2.jpg"
            ],
            "imgAvatar": "1.u5367.d20170630.t111628.609739_2.jpg"
        },
        {
            "id": "46",
            "level_1": 1,
            "level_2": 16,
            "sku": "5805156212278",
            "name": "Điện Thoại Huawei Y5 2017 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Viễn Tin Sài Gòn",
            "star": 5,
            "price": "4280000",
            "marketPrice": "2990000",
            "comment": "10",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: IPS 5.5 inch (HD 720 x 1280 pixels)",
                "Camera Trước/Sau: 8MP/12MP"
            ],
            "imgUrl": [
                "1.u5552.d20170907.t160845.904185.jpg",
                "2.u5552.d20170907.t160845.955532.jpg",
                "3.u5552.d20170907.t160846.1602.jpg"
            ],
            "imgAvatar": "1.u5552.d20170907.t160845.904185.jpg"
        },
        {
            "id": "47",
            "level_1": 1,
            "level_2": 17,
            "sku": "4809366888354",
            "name": "iPad WiFi 32GB New 2017 - Hàng Chính Hãng",
            "trademark": " Apple",
            "commitmentName": "Nguyễn Hoàng",
            "star": 4,
            "price": "7750000",
            "marketPrice": "8999000",
            "comment": "15",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: LED backlit LCD, 9.7 inch",
                "CPU: Apple A9 2 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u2769.d20170623.t134206.993432 (1).jpg",
                "2.u2769.d20170623.t134207.53662.jpg",
                "3.u2769.d20170623.t134207.100790.jpg"
            ],
            "imgAvatar": "1.u2769.d20170623.t134206.993432.jpg"
        },
        {
            "id": "48",
            "level_1": 1,
            "level_2": 17,
            "sku": "4809366888354",
            "name": "Điện Thoại iPhone X 256GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "29400000",
            "marketPrice": "34790000",
            "comment": "10",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mã Quốc Tế: LL/ZA/ZP/..",
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Camera Trước/Sau: 7MP/ 2 camera 12MP"
            ],
            "imgUrl": [
                "a0f3f0400838ac900b1fa1bd48e4f636 (1).jpg",
                "2.u4939.d20170918.t113145.543503_3_3_3_3.jpg",
                "3.u4939.d20170918.t113145.578791_3_3_3_3.jpg"
            ],
            "imgAvatar": "a0f3f0400838ac900b1fa1bd48e4f636.jpg"
        },
        {
            "id": "49",
            "level_1": 1,
            "level_2": 17,
            "sku": "5805871271802",
            "name": "Điện Thoại iPhone 6s 32GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "12100000",
            "marketPrice": "12990000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Màn hình: 4.7 inch, HD (1334 x 750 Pixels)"
            ],
            "imgUrl": [
                "32gbgray_1.u504.d20161031.t112302.986490 (1).jpg",
                "32gbgray_2.u504.d20161031.t112303.34907.jpg",
                "32gbgray_3.u504.d20161031.t112303.75300.jpg"
            ],
            "imgAvatar": "32gbgray_1.u504.d20161031.t112302.986490.jpg"
        },
        {
            "id": "50",
            "level_1": 1,
            "level_2": 17,
            "sku": "5801713328154",
            "name": "Điện Thoại iPhone 6s Plus 32GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "iStore",
            "star": 4,
            "price": "13500000",
            "marketPrice": "14990000",
            "comment": "20",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%, chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Camera Trước/Sau: 5MP/ 12MP"
            ],
            "imgUrl": [
                "iphone-6s-plus-silver-1.u504.d20161031.t111819.627055 (1).jpg",
                "iphone-6s-plus-silver-2.u504.d20161031.t111819.664957.jpg",
                "iphone-6s-plus-silver-3.u504.d20161031.t111819.690656.jpg"
            ],
            "imgAvatar": "iphone-6s-plus-silver-1.u504.d20161031.t111819.627055.jpg"
        },
        {
            "id": "51",
            "level_1": 1,
            "level_2": 18,
            "sku": "5807747664701",
            "name": "Điện Thoại Asus ZenFone 4 Max Pro (ZC554KL) - Hàng Chính Hãng",
            "trademark": " Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4090000",
            "marketPrice": "4990000",
            "comment": "20",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 5.5 inch (720 x 1280 pixels)",
                "Dung lượng Pin: 5000 mAh"
            ],
            "imgUrl": [
                "1.u4939.d20170822.t155930.280439.jpg",
                "2.u4939.d20170822.t155930.319988.jpg",
                "3.u4939.d20170822.t155930.350543.jpg"
            ],
            "imgAvatar": "1.u4939.d20170822.t155930.280439.jpg"
        },
        {
            "id": "52",
            "level_1": 1,
            "level_2": 18,
            "sku": "9452644023890",
            "name": "Điện Thoại Asus ZenFone 4 Max Pro (ZC554KL) - Hàng Chính Hãng",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4790000",
            "marketPrice": "5490000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Kim loại nguyên khối",
                "Camera Trước/Sau: 8MP và 16MP/8MP"
            ],
            "imgUrl": [
                "930f1f900a75cb52e671ebb4f23bc261.jpg",
                "c61dee238b14e187bb40b6650e5fece9.jpg",
                "9c24bb351cb09f43b6c848a3063a663d.jpg"
            ],
            "imgAvatar": "930f1f900a75cb52e671ebb4f23bc261.jpg"
        },
        {
            "id": "53",
            "level_1": 1,
            "level_2": 18,
            "sku": "5068657566893",
            "name": "Điện Thoại Asus ZenFone 4 Max Pro (ZC554KL) - Hàng Chính Hãng",
            "trademark": "Asus",
            "commitmentName": "Khoa Nguyễn",
            "star": 4,
            "price": "3490000",
            "marketPrice": "3990000",
            "comment": "17",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Camera Trước/Sau: 8MP/ 2 camera 5MP và 13MP",
                "CPU: Qualcomm Snapdragon 425 4 nhân 64-bit",
                "Tính năng: Cảm biến vân tay một chạm"
            ],
            "imgUrl": [
                "2b0e506e2daea1d93ac6ca39cdb3cb07.jpg",
                "24347c736afbc116496e3a75fe4feb58.jpg",
                "6915d56ba63c1c5fd5550f70a3c5de6d.jpg"
            ],
            "imgAvatar": "2b0e506e2daea1d93ac6ca39cdb3cb07.jpg"
        },
        {
            "id": "54",
            "level_1": 1,
            "level_2": 18,
            "sku": "5809028570435",
            "name": "Điện Thoại Asus ZenFone 4 Max Pro (ZC554KL) Và Tai Nghe Samsung Galaxy S7 - Hàng Chính Hãng",
            "trademark": "Asus",
            "commitmentName": "Khoa Nguyễn",
            "star": 4,
            "price": "4400000 ",
            "marketPrice": "5290000",
            "comment": "9",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Màn hình: 5.5 inch (720 x 1280 pixels)",
                "Camera Trước/Sau: 16MP/ 2 camera 16MP",
                "Tính năng: Mở khóa bằng vân tay, sạc pin cho thiết bị khác, hỗ trợ thẻ nhớ ngoài"
            ],
            "imgUrl": [
                "0.u4939.d20170920.t102339.302909.jpg",
                "2-u4939-d20170822-t155930-319988.u4939.d20170920.t102339.371532.jpg",
                "3-u4939-d20170822-t155930-350543.u4939.d20170920.t102339.399436.jpg"
            ],
            "imgAvatar": "0.u4939.d20170920.t102339.302909.jpg"
        },
        {
            "id": "55",
            "level_1": 1,
            "level_2": 19,
            "sku": "5808622383403",
            "name": "Điện Thoại Xiaomi Redmi 4X (16GB/2GB) - Hàng Nhập Khẩu",
            "trademark": " Xiaomi",
            "commitmentName": "Huy Nguyễn",
            "star": 4,
            "price": "2530000",
            "marketPrice": "2990000 ",
            "comment": "9",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối",
                "Camera Trước/Sau: 5MP/ 13MP"
            ],
            "imgUrl": [
                "1.u2769.d20170511.t143701.195038_4_3.jpg",
                "2.u2769.d20170511.t143701.230906_4_3.jpg",
                "3.u2769.d20170511.t143701.260749_4_3.jpg"
            ],
            "imgAvatar": "1.u2769.d20170511.t143701.195038_4_3.jpg"
        },
        {
            "id": "56",
            "level_1": 1,
            "level_2": 19,
            "sku": "4808312930154",
            "name": "Máy Tính Bảng Mobell Tab 8s - Hàng Chính Hãng﻿",
            "trademark": "Mobell",
            "commitmentName": "Huy Nguyễn",
            "star": 4,
            "price": "2190000 ",
            "marketPrice": "2490000",
            "comment": "9",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: IPS LCD, 8 inch",
                "Camera Trước/Sau: 2MP/ 8 MP"
            ],
            "imgUrl": [
                "1.u2769.d20170729.t144605.526894.jpg",
                "2.u2769.d20170729.t144605.574985.jpg",
                "3.u2769.d20170729.t144605.614230.jpg"
            ],
            "imgAvatar": "1.u2769.d20170729.t144605.526894.jpg"
        },
        {
            "id": "57",
            "level_1": 1,
            "level_2": 19,
            "sku": "5800617486335",
            "name": "Điện Thoại Mobiistar Lai Zoro 2 - Hàng Chính Hãng",
            "trademark": "Mobiistar",
            "commitmentName": "Huy Nguyễn",
            "star": 4,
            "price": "2190000 ",
            "marketPrice": "2490000",
            "comment": "19",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Pin rời",
                "Màn hình: TFT, 5 inch FWVGA",
                "SIM: SIM thường và Micro SIM"
            ],
            "imgUrl": [
                "lai zoro 2 white_1.u2470.d20160626.t135635.jpg",
                "lai zoro 2 white_2.u2470.d20160626.t135635.jpg",
                "lai zoro 2 black_2.u2470.d20160626.t135315.jpg"
            ],
            "imgAvatar": "lai zoro 2 white_1.u2470.d20160626.t135635.jpg"
        },
        {
            "id": "58",
            "level_1": 1,
            "level_2": 19,
            "sku": "5807961716392",
            "name": "Bluboo Dual (16GB) - Hàng Chính Hãng",
            "trademark": " Bluboo",
            "commitmentName": "Huy Trần",
            "star": 4,
            "price": "2190000 ",
            "marketPrice": "2490000",
            "comment": "6",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màn hình: 5.5” Full HD (1920 x 1080 pixels)",
                "CPU: 4 nhân 1.5 Ghz 64 bits",
                "Camera sau: 13.0 MP + 2MP; Camera trước: 8 MP",
                "HĐH: Android 6.0 Marshmallow"
            ],
            "imgUrl": [
                "1.u2751.d20170602.t162705.441518.jpg",
                "3.u2751.d20170602.t162705.501539.jpg",
                "4.u2751.d20170602.t162705.527355.jpg"
            ],
            "imgAvatar": "1.u2751.d20170602.t162705.441518.jpg"
        },
        {
            "id": "59",
            "level_1": 1,
            "level_2": 20,
            "sku": "5804639338177",
            "name": "Điện Thoại OPPO A37 (Neo 9) - Hàng Chính Hãng",
            "trademark": "OPPO",
            "commitmentName": "Huy Trần",
            "star": 4,
            "price": "3290000",
            "marketPrice": "3690000",
            "comment": "6",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, mặt kính cong 2.5D",
                "Camera Trước/Sau: 5MP/8MP"
            ],
            "imgUrl": [
                "a37 gold_1.u504.d20160722.t121614.jpg",
                "a37 gold_2.u504.d20160722.t121614.jpg",
                "a37 gold_3.u504.d20160722.t121614.jpg"
            ],
            "imgAvatar": "a37 gold_1.u504.d20160722.t121614.jpg"
        },
        {
            "id": "60",
            "level_1": 1,
            "level_2": 20,
            "sku": "5805360997121",
            "name": "Điện Thoại Samsung Galaxy J3 Pro - Hàng Chính Hãng",
            "trademark": "Samsung",
            "commitmentName": "Huy Trần",
            "star": 4,
            "price": "3990000",
            "marketPrice": "4490000",
            "comment": "6",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối",
                "Màn hình: PLS TFT LCD, 5 inch"
            ],
            "imgUrl": [
                "11000263_1_1.u3059.d20170802.t145701.534627.jpg",
                "11000263_3.u3059.d20170802.t145701.566821.jpg",
                "2.u2769.d20170721.t091620.831694_3.jpg"
            ],
            "imgAvatar": "11000263_1_1.u3059.d20170802.t145701.534627.jpg"
        },
        {
            "id": "61",
            "level_1": 1,
            "level_2": 20,
            "sku": "5801059476489",
            "name": "Điện Thoại Xiaomi Mi A1 64GB/4GB - Hàng Chính Hãng DGW",
            "trademark": " Xiaomi",
            "commitmentName": "Như Nguyễn",
            "star": 4,
            "price": "4950000",
            "marketPrice": "5990000",
            "comment": "6",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Camera Trước/Sau: 5 MP/ 2 camera 12 MP",
                "CPU: Snapdragon 625 8 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u4939.d20170926.t140931.179233_1.jpg",
                "1_1.u4939.d20170926.t140931.216367_1.jpg",
                "1_2.u4939.d20170926.t140931.246487_1.jpg"
            ],
            "imgAvatar": "1.u4939.d20170926.t140931.179233_1.jpg"
        },
        {
            "id": "62",
            "level_1": 1,
            "level_2": 20,
            "sku": "5805433793124",
            "name": "Điện Thoại OPPO F3 Lite (A57) - Hàng Chính Hãng",
            "trademark": "OPPO",
            "commitmentName": "Như Nguyễn",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5490000",
            "comment": "6",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nguyên khối, viền màn hình được làm cong 2.5D",
                "Camera Trước/Sau: 16MP/13MP"
            ],
            "imgUrl": [
                "1.u5488.d20170724.t140050.815458_1 (1).jpg",
                "2.u5488.d20170724.t140050.874658_1.jpg",
                "3.u5488.d20170724.t140050.898190_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170724.t140050.815458_1 (1).jpg"
        },
        {
            "id": "63",
            "level_1": 1,
            "level_2": 21,
            "sku": "5805438792511",
            "name": "Điện Thoại Sony Xperia XZ F8332 - Hàng Chính Hãng",
            "trademark": " Sony",
            "commitmentName": "Huỳnh Như",
            "star": 4,
            "price": "8990000",
            "marketPrice": "12990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màn hình: 5.2 Full HD 1080p",
                "Camera trước/sau: 13MP/23MP",
                "RAM: 3 GB",
                "ROM: 64 GB"
            ],
            "imgUrl": [
                "sony-xzden.u504.d20161003.t161127.398838.jpg",
                "sony-xzden1.u504.d20161003.t161127.440181.jpg",
                "sony-xzden2.u504.d20161003.t161127.476914.jpg"
            ],
            "imgAvatar": "sony-xzden.u504.d20161003.t161127.398838.jpg"
        },
        {
            "id": "64",
            "level_1": 1,
            "level_2": 21,
            "sku": "5805438792511",
            "name": "Sony Xperia X - F5122",
            "trademark": " Sony",
            "commitmentName": "Huỳnh Như",
            "star": 4,
            "price": "9190000",
            "marketPrice": "11990000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màn hình: 5.2 Full HD 1080p",
                "Camera trước/sau: 13MP/23MP",
                "RAM: 3 GB",
                "ROM: 64 GB"
            ],
            "imgUrl": [
                "xperiaxlime1.u2470.d20160602.t111037.jpg",
                "xperiaxlime2.u2470.d20160602.t111038.jpg",
                "xperiaxlime3.u2470.d20160602.t111038.jpg"
            ],
            "imgAvatar": "xperiaxlime1.u2470.d20160602.t111037.jpg"
        },
        {
            "id": "65",
            "level_1": 1,
            "level_2": 21,
            "sku": "5800001591645",
            "name": "Điện Thoại BlackBerry Priv - Hàng Chính Hãng",
            "trademark": "BlackBerry",
            "commitmentName": "Huỳnh Như",
            "star": 4,
            "price": "8990000",
            "marketPrice": "11990000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hàng chính hãng, mới 100%",
                "Màn hình: 5.4 (2560 x 1440 pixels)",
                "Camera trước/sau: 2 MP/18 MP",
                "Pin: 3410 mAh"
            ],
            "imgUrl": [
                "priv_1.u504.d20170104.t155614.865271.jpg",
                "priv_3.u504.d20170104.t155614.937457.jpg",
                "priv_4.u504.d20170104.t155614.967633.jpg"
            ],
            "imgAvatar": "priv_1.u504.d20170104.t155614.865271.jpg"
        },
        {
            "id": "66",
            "level_1": 1,
            "level_2": 21,
            "sku": "5804995975474",
            "name": "Điện Thoại Motorola Moto Z2 Play - Hàng Chính Hãng",
            "trademark": "Motorola",
            "commitmentName": "Huỳnh Như",
            "star": 4,
            "price": "9650000",
            "marketPrice": "10990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Thiết kế: Kim loại nguyên khối",
                "Camera Trước/Sau: 5MP/12MP",
                "CPU: Vi xử lý 8 lõi Snapdragon 626 2.2GHz"
            ],
            "imgUrl": [
                "1.u5395.d20170717.t153133.429002_2.jpg",
                "2.u5395.d20170717.t153133.463424_2.jpg",
                "3.u5395.d20170717.t153133.494952_2.jpg"
            ],
            "imgAvatar": "1.u5395.d20170717.t153133.429002_2.jpg"
        },
        {
            "id": "67",
            "level_1": 2,
            "level_2": 2,
            "sku": "8908348106113",
            "name": "Smart Tivi Cong 4K Samsung 43 inch UA43KU6500",
            "trademark": "Samsung",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "11990000",
            "marketPrice": "18390000 ",
            "comment": "12",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại tivi: Smart Tivi Cong HDR",
                "Màn hình: 43 inch",
                "Độ phân giải: Ultra HD 4K",
                "Kết nối: HDMI, USB, Wifi"
            ],
            "imgUrl": [
                "30af8543ccc8f3b258ee898c3c326088.jpg",
                "ua43ku65001.u2470.d20160912.t171211.811205.jpg",
                "ua43ku65003.u2470.d20160912.t171211.879205.jpg"
            ],
            "imgAvatar": "30af8543ccc8f3b258ee898c3c326088.jpg"
        },
        {
            "id": "68",
            "level_1": 2,
            "level_2": 3,
            "sku": "8903676142925",
            "name": "Smart Tivi LG 43 inch 4K UHD 43UJ632T",
            "trademark": "LG",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": " 8890000",
            "marketPrice": "12900000",
            "comment": "44",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tivi 4K, 43 inch",
                "Độ phân giải: 4K UHD",
                "Hệ điều hành: WebOS 3.5",
                "Kết nối: Wifi, LAN, HDMI, Bluetooth"
            ],
            "imgUrl": [
                "4119679b7e940a26594626104ef71c27.jpg",
                "2.u5395.d20170720.t174440.633292.jpg",
                "3.u5395.d20170720.t174440.693993.jpg"
            ],
            "imgAvatar": "4119679b7e940a26594626104ef71c27.jpg"
        },
        {
            "id": "69",
            "level_1": 2,
            "level_2": 4,
            "sku": "8908500266563",
            "name": "Smart Tivi Panasonic 32 inch TH-32ES500V",
            "trademark": "Panasonic",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5390000",
            "marketPrice": "7600000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màn hình: Smart Tivi 32 inch, HD (1366 x 768 px)",
                "Tần số quét: BMR 800Hz",
                "Chức năng khử nhiễu hạt giúp hình ảnh rõ nét hơn",
                "Công nghệ hình ảnh: Hexa Chroma Drive, LED IPS"
            ],
            "imgUrl": [
                "th-32es500v-.u2769.d20170531.t022340.908977.jpg",
                "th-32es500v-2.u2769.d20170531.t022340.946115.jpg",
                "3.u5395.d20170720.t174440.693993.jpg"
            ],
            "imgAvatar": "th-32es500v-3.u2769.d20170531.t022340.975434.jpg"
        },
        {
            "id": "70",
            "level_1": 2,
            "level_2": 5,
            "sku": "8908500266563",
            "name": "Tivi Toshiba 32 inch 32L3750",
            "trademark": " Toshiba",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "4490000",
            "marketPrice": "5490000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế thanh mảnh, màn hình 32 inch sắc nét",
                "Công nghệ Essential PQ Technology cho màu sắc rực rỡ",
                "Công nghệ AMR+200 cho hiển thị những pha chuyển động nhanh sắc nét hơn",
                "Âm thanh vòm ảo sống động"
            ],
            "imgUrl": [
                "1.u3059.d20170910.t120754.487485.jpg",
                "2.u3059.d20170910.t120754.521901.jpg",
                "4.u3059.d20170910.t120754.549757.jpg"
            ],
            "imgAvatar": "1.u3059.d20170910.t120754.487485.jpg"
        },
        {
            "id": "71",
            "level_1": 2,
            "level_2": 6,
            "sku": "1292725227223",
            "name": "Dàn Âm Thanh Sony 5.1 kênh BDV-E2100",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "4780000",
            "marketPrice": "5190000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hệ thống loa: 5.1 kênh",
                "Tổng công suất: 1000 W",
                "Kết nối có dây: Cổng LAN, USB, HDMI, Optical",
                "Kết nối không dây: NFC, Bluetooth, Wifi"
            ],
            "imgUrl": [
                "dan-may-blu-ray-sony-bdv-e2100-msp1-5.jpg",
                "dan-may-blu-ray-sony-bdv-e2100-msp1-6.jpg",
                "dan-may-blu-ray-sony-bdv-e2100-msp1-4.jpg"
            ],
            "imgAvatar": "dan-may-blu-ray-sony-bdv-e2100-msp1-5.jpg"
        },
        {
            "id": "72",
            "level_1": 2,
            "level_2": 7,
            "sku": "1296997770944",
            "name": "Loa Kéo MBA F-15A",
            "trademark": "MBA",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "2390000",
            "marketPrice": "3990000 ",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 300 W",
                "Bass: 15 ",
                "Thời lượng pin: 5-7 giờ",
                ""
            ],
            "imgUrl": [
                "loa-mba-f-15a--1-.u2470.d20161215.t150440.546009.jpg",
                "loa-mba-f-15a--2-.u2470.d20161215.t150440.579577.jpg",
                "loa-mba-f-15a--3-.u2470.d20161215.t150440.604119.jpg"
            ],
            "imgAvatar": "loa-mba-f-15a--1-.u2470.d20161215.t150440.546009.jpg"
        },
        {
            "id": "73",
            "level_1": 2,
            "level_2": 8,
            "sku": "2748076774712",
            "name": "Khung Treo Tivi Nghiêng 37 Inch - 63 Inch",
            "trademark": "CP",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "210000",
            "marketPrice": "219000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khung treo Tivi nghiêng 37 - 63 inch",
                "Nghiêng 15 độ - Ngang 650mm - Cao 405mm",
                "Khả năng chịu tải: tối đa 65kg",
                "Mang lại sự thẩm mỹ"
            ],
            "imgUrl": [
                "khung-treo-tivi-nghieng-37-icnh---63-inch-1.u504.d20161101.t102319.834336.jpg",
                "khung-treo-tivi-nghieng-37-icnh---63-inch-2.u504.d20161101.t102319.965685.jpg"
            ],
            "imgAvatar": "khung-treo-tivi-nghieng-37-icnh---63-inch-1.u504.d20161101.t102319.834336.jpg"
        },
        {
            "id": "74",
            "level_1": 2,
            "level_2": 8,
            "sku": "2748076774712",
            "name": "Khung Treo Tivi Nghiêng 37 Inch - 63 Inch",
            "trademark": "CP",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "210000",
            "marketPrice": "219000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khung treo Tivi nghiêng 37 - 63 inch",
                "Nghiêng 15 độ - Ngang 650mm - Cao 405mm",
                "Khả năng chịu tải: tối đa 65kg",
                "Mang lại sự thẩm mỹ"
            ],
            "imgUrl": [
                "khung-treo-tivi-nghieng-37-icnh---63-inch-1.u504.d20161101.t102319.834336.jpg",
                "khung-treo-tivi-nghieng-37-icnh---63-inch-2.u504.d20161101.t102319.965685.jpg"
            ],
            "imgAvatar": "khung-treo-tivi-nghieng-37-icnh---63-inch-1.u504.d20161101.t102319.834336.jpg"
        },
        {
            "id": "75",
            "level_1": 2,
            "level_2": 9,
            "sku": "2748076774712",
            "name": "Amply Denon PMA520AESPE2",
            "trademark": "Denon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "8590000",
            "marketPrice": "8810000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất tiêu thụ: 185W",
                "Mạch Single-Push-Pull Circuit",
                "Mạch Loudness",
                "Mặt nhôm phay sang trọng"
            ],
            "imgUrl": [
                "pma520aespe2_1.u2470.d20160712.t095907.jpg",
                "pma520aespe2_2.u2470.d20160712.t095907.jpg",
                "pma520aespe2_3.u2470.d20160712.t095907.jpg"
            ],
            "imgAvatar": "pma520aespe2_1.u2470.d20160712.t095907.jpg"
        },
        {
            "id": "76",
            "level_1": 2,
            "level_2": 10,
            "sku": "6919388799533",
            "name": "Tivi LED Darling 24 inch HD 24HD900T2",
            "trademark": "CP",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "2290000",
            "marketPrice": " 3150000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại tivi: Tivi LED, 24 inch",
                "Độ phân giải: 1366 x 768px",
                "Kết nối: USB, HDMI",
                "Công nghệ hình ảnh: Đèn LED nền"
            ],
            "imgUrl": [
                "590180e23c93631f8b1233ea2fb305a8.jpg",
                "9a26911ed9003c69a3db8fc97b74e426.jpg"
            ],
            "imgAvatar": "590180e23c93631f8b1233ea2fb305a8.jpg"
        },
        {
            "id": "77",
            "level_1": 2,
            "level_2": 11,
            "sku": "8902860669583",
            "name": "Tivi LED Sharp 32 inch LC-32LE280X",
            "trademark": "Sharp",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3690000",
            "marketPrice": "4800000",
            "comment": "7",
            "residual": "40",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại Tivi: Tivi LED, 32 inch",
                "Độ phân giải: HD ready (1366 x 768 pixels)",
                "Tần số quét: 200",
                "Cổng kết nối: 2 HMDI, 1 USB"
            ],
            "imgUrl": [
                "32le280x-.u4064.d20170401.t221213.721363.jpg",
                "32le280x-1.u4064.d20170401.t221213.760722.jpg",
                "32le280x-2.u4064.d20170401.t221213.788264.jpg"
            ],
            "imgAvatar": "32le280x-.u4064.d20170401.t221213.721363.jpg"
        },
        {
            "id": "78",
            "level_1": 2,
            "level_2": 12,
            "sku": "8902860669583",
            "name": "Smart Tivi 4K Skyworth 50 inch 50K820S",
            "trademark": "Skyworth",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "8890000 ",
            "marketPrice": "12990000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại tivi: Smart LED 4K",
                "Kích thước: 50 inch",
                "Tần số quét: 100 Hz",
                "Kết nối: HDMI, USB"
            ],
            "imgUrl": [
                "8906974272776-u2470-d20160929-t115625-359621.u3059.d20170621.t103203.976761.jpg",
                "8906974272776_1-u2470-d20160929-t115625-403216.u3059.d20170621.t103204.25499.jpg",
                "8906974272776_2-u2470-d20160929-t115625-438639.u3059.d20170621.t103204.61501.jpg"
            ],
            "imgAvatar": "8906974272776-u2470-d20160929-t115625-359621.u3059.d20170621.t103203.976761.jpg"
        },
        {
            "id": "79",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "80",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "81",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "82",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "83",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "84",
            "level_1": 3,
            "level_2": 1,
            "sku": "8902860669583",
            "name": "Tai Nghe Nhét Tai Langsdom Super Bass JM26 - Hàng Nhập Khẩu",
            "trademark": "Langston",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "75000",
            "marketPrice": "118000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế gọn nhẹ, tiện dụng",
                "Trở kháng: 16 Ohms",
                "Tần số: 20Hz - 20000Hz",
                "Độ nhạy: 101dB"
            ],
            "imgUrl": [
                "1.u5488.d20170911.t104116.803809_1.jpg",
                "2.u5488.d20170911.t104116.865563_1.jpg",
                "3.u5488.d20170911.t104116.911701_1.jpg"
            ],
            "imgAvatar": "1.u5488.d20170911.t104116.803809_1.jpg"
        },
        {
            "id": "85",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "86",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "87",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "88",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "89",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "90",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "91",
            "level_1": 3,
            "level_2": 2,
            "sku": "6505613864116",
            "name": "Tai Nghe Sony MDR-ZX110AP Chụp Tai",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "569000",
            "marketPrice": "590000",
            "comment": "64",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Tích hợp micro tiện lợi",
                "Thiết kế gọn nhẹ",
                "Chất lượng âm thanh tốt",
                "Chuẩn kết nối 3.5mm"
            ],
            "imgUrl": [
                "tr_ng-1_9_1_.jpg",
                "img_210.u2470.d20170317.t123842.929911.jpg",
                "img_2102.u2470.d20170317.t123842.981168.jpg"
            ],
            "imgAvatar": "tr_ng-1_9_1_.jpg"
        },
        {
            "id": "92",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "93",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "94",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "95",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "96",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "97",
            "level_1": 3,
            "level_2": 3,
            "sku": "6508960107011",
            "name": "Tai Nghe Bluetooth Plantronics Explorer 10 - Hàng Chính Hãng",
            "trademark": "Plantronics",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "590000 ",
            "marketPrice": "790000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công nghệ bluetooth: Version 4.1",
                "Phạm vi kết nối: 10m",
                "Thời gian đàm thoại liên tục: 7h",
                "Khả năng lọc tiếng ồn, tiếng gió và tiếng vang"
            ],
            "imgUrl": [
                "1.u2769.d20170407.t161114.526045.jpg",
                "2.u2769.d20170407.t161114.563618.jpg",
                "3.u2769.d20170407.t161114.599200.jpg"
            ],
            "imgAvatar": "1.u2769.d20170407.t161114.526045.jpg"
        },
        {
            "id": "98",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "99",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "100",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "101",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "102",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "103",
            "level_1": 3,
            "level_2": 4,
            "sku": "8206702639612",
            "name": "Tai Nghe Bluetooth Nhét Tai Anker SoundBuds Surge A3236 - Hàng Chính Hãng",
            "trademark": "Anker",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "699000",
            "marketPrice": "850000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Đạt tiêu chuẩn IPX 4",
                "Âm thanh chất lượng cao",
                "Công nghệ hủy bỏ tiếng ồn CVC 6.0",
                "Phù hợp người thích hoạt động thể thao"
            ],
            "imgUrl": [
                "85873b0b1d3eaf4b1542ddff7696ebfa.jpg",
                "18f4ae4e4426bd840a82f03a8804d1ed.jpg",
                "7f50c8c90648f1d7ebb6a8e182c4a697.jpg"
            ],
            "imgAvatar": "85873b0b1d3eaf4b1542ddff7696ebfa.jpg"
        },
        {
            "id": "104",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "105",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "106",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "107",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "108",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "109",
            "level_1": 3,
            "level_2": 5,
            "sku": " 6509520044913",
            "name": "Tai Nghe Bluetooth Jabra Halo Smart",
            "trademark": "Jabra",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1190000",
            "marketPrice": "1800000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Khả năng chống vấy nước",
                "Micro có khả năng loại bỏ tiếng gió",
                "Thời gian đàm thoại lên đến 17 giờ",
                "Kết nối với Siri and Google Now chỉ bằng 1 chạm"
            ],
            "imgUrl": [
                "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg",
                "jabra-halo-smart-blue_2.u2470.d20170206.t120242.377119.jpg",
                "jabra-halo-smart-blue_3.u2470.d20170206.t120242.406211.jpg"
            ],
            "imgAvatar": "jabra-halo-smart-blue.u2470.d20170206.t120242.300968.jpg"
        },
        {
            "id": "110",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "111",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "112",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "113",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "114",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "115",
            "level_1": 3,
            "level_2": 6,
            "sku": "6705547365097",
            "name": "Chuột Chơi Game Logitech G403 Prodigy",
            "trademark": "Logitech",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1110000",
            "marketPrice": "1599000 ",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến quang học Pixart PMW3366",
                "Thiết kế khoang gắn tạ thông minh",
                "Hệ thống LED scroll và LED logo RGB đổi màu tùy ý",
                "Dây bọc dù chống đứt"
            ],
            "imgUrl": [
                "g403-co-day_1.u2470.d20161025.t150655.225369.jpg",
                "g403-co-day_2.u2470.d20161025.t150655.276962.jpg",
                "g403-co-day_3.u2470.d20161025.t150655.321853.jpg"
            ],
            "imgAvatar": "g403-co-day_1.u2470.d20161025.t150655.225369.jpg"
        },
        {
            "id": "116",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "117",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "118",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "119",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "120",
            "level_1": 3,
            "level_2": 7,
            "sku": "5587892539276",
            "name": "Bàn Phím Game Có Dây DareU LK160 LED Membrane Full-size - Hàng Chính Hãng",
            "trademark": "DareU",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "279000",
            "marketPrice": "366000",
            "comment": "6",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Bàn phím 104 phím",
                "5 hiệu ứng đèn led",
                "Có kệ để tay",
                "Dây dài 1.8 mét"
            ],
            "imgUrl": [
                "1.u5552.d20170911.t181943.619353 (1).jpg",
                "2.u5552.d20170911.t181943.649015.jpg",
                "3.u5552.d20170911.t181943.687179.jpg"
            ],
            "imgAvatar": "1.u5552.d20170911.t181943.619353 (1).jpg"
        },
        {
            "id": "121",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "122",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "123",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "124",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "125",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "126",
            "level_1": 3,
            "level_2": 8,
            "sku": "7272502895948",
            "name": "Ghế Chơi Game DXRacer Formula Series - Hàng Chính Hãng",
            "trademark": "DXRacer",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5999000",
            "marketPrice": "6999000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu: Nỉ và da PU",
                "Kê tay: Có thể nâng lên, hạ xuống",
                "Khung chân: Nhôm nguyên khối",
                "Phụ kiện đi kèm: Đệm kê lưng, đệm kê đầu"
            ],
            "imgUrl": [
                "2372fcd149d9a7743d9f5f1d5a334ac4.jpg",
                "78a334735f1c12ea23520da044da891a.jpg",
                "80af17f2676bcc9fee4941e9ef342acc.jpg"
            ],
            "imgAvatar": "2372fcd149d9a7743d9f5f1d5a334ac4.jpg"
        },
        {
            "id": "127",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "128",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "129",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "130",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "131",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "132",
            "level_1": 3,
            "level_2": 9,
            "sku": "7005930683821",
            "name": "Miếng Lót Chuột SteelSeries QcK Mini - Hàng Chính Hãng",
            "trademark": "SteelSeries",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "169000",
            "marketPrice": "188000",
            "comment": "7",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chất liệu cao cấp",
                "Kích thước lớn",
                "Bề mặt vải mịn",
                "Thiết kế đẹp, hiện đại"
            ],
            "imgUrl": [
                "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg",
                "steelseries-qck-boxshot_1.jpg",
                "26.ban di chuot steelseries qck mini - gaming (2).u2470.d20160516.t104242.jpg"
            ],
            "imgAvatar": "26.ban di chuot steelseries qck mini - gaming (1).u2470.d20160516.t104242.jpg"
        },
        {
            "id": "133",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "134",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "135",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "136",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "137",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "138",
            "level_1": 3,
            "level_2": 10,
            "sku": "7509369048256",
            "name": "TP-Link TL-WR940N - Router Wifi Chuẩn N Tốc Độ 450Mbps",
            "trademark": "TP-Link",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "469000 ",
            "marketPrice": "600000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Pin sạc 2000mAH",
                "Tốc độ WiFi 300Mbps",
                "Hoạt động trên băng tần 2.4GHz/5GHz",
                "Màn hình hiển thị TFT 1.4 inch"
            ],
            "imgUrl": [
                "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg",
                "tl-wr940n_un_v1_280_68_large_2.00_20150213133749_1.jpg",
                "tl-wr940n_un_v1_280_68_large_3.00_20150213133812_1.jpg"
            ],
            "imgAvatar": "tl-wr940n_un_v1_280_68_large_1.00_20150213133659_1.jpg"
        },
        {
            "id": "139",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "140",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "141",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "142",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "143",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "144",
            "level_1": 3,
            "level_2": 11,
            "sku": "7505177418601",
            "name": "Huawei Wifi Di Động 4G E5573Cs-322 - Hàng Chính Hãng",
            "trademark": "Huawei",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1150000",
            "marketPrice": "1550000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Hỗ trợ 3G, 4G, LTE",
                "Chuẩn Wi-Fi 802.11 a/b/g/n",
                "Tốc độ 100/50Mbps",
                "Kết nối tối đa 16 thiết bị"
            ],
            "imgUrl": [
                "4787475f163140320fe758fa5ee97c76.jpg",
                "e6273de153ff8acae4834a94950ba45b.jpg",
                "7e0d10877fb02a84a6ff764a85f92eb6.jpg"
            ],
            "imgAvatar": "4787475f163140320fe758fa5ee97c76.jpg"
        },
        {
            "id": "145",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "146",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "147",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "148",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "149",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "150",
            "level_1": 3,
            "level_2": 12,
            "sku": "7506828761039",
            "name": "TP-Link TL-WN822N - USB Wifi (high gain) chuẩn N tốc độ 300Mbps",
            "trademark": "TP-Link",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "244000",
            "marketPrice": "325000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng để kết nối một máy tính để bàn hoặc máy tính xách tay",
                "Tương thích với các thiết bị 802.11b/g/n và tốc độ truyền lên đến 300Mbps",
                "Được trang bị ăng ten kép bên ngoài 3 dBi có khả năng xoay",
                "Dễ dàng cài đặt và cấu hình với đĩa CD cài đặt và tiện ích"
            ],
            "imgUrl": [
                "tl-wn822n.jpg",
                "tl-wn822n1.jpg",
                "tl-wn822n2.jpg"
            ],
            "imgAvatar": "tl-wn822n.jpg"
        },
        {
            "id": "151",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "152",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "153",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "154",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "155",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "156",
            "level_1": 4,
            "level_2": 1,
            "sku": "5300437776864",
            "name": "Macbook Air 2017 MQD32 (13.3 inch) - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "NPP IT Chính Hãng",
            "star": 4,
            "price": "21010000",
            "marketPrice": "24750000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5 Dual-core 1.8 GHz",
                "RAM: 8GB 1600 MHz LPDDR3",
                "Ổ cứng: 128GB PCIe-Based Flash",
                "Chipset đồ họa: Intel HD Graphics 6000"
            ],
            "imgUrl": [
                "1.u5395.d20170714.t174351.947380.jpg",
                "2.u5395.d20170714.t174351.352.jpg",
                "3.u5395.d20170714.t174352.40164.jpg"
            ],
            "imgAvatar": "1.u5395.d20170714.t174351.947380.jpg"
        },
        {
            "id": "157",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "158",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "159",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "160",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "161",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "162",
            "level_1": 4,
            "level_2": 2,
            "sku": "2914049808610",
            "name": "Laptop Dell Latitude E7270 70144919 Core i5-6300U/Win 10 (12.5 inch) - Black - Hàng Chính Hãng",
            "trademark": "Dell",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "22990000",
            "marketPrice": "26990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-6300U",
                "RAM: 8GB",
                "Ổ cứng: 256GB",
                "Màn hình: Cảm ứng 12.5 inch Full HD (1920 x 1080) LCD"
            ],
            "imgUrl": [
                "a9f64bbf18bb4567952f8ab979f7734a.jpg",
                "3672249ac21e19fc7d5212f60ae22b86.jpg",
                "15c3a28a96eceff324d4054dda7c1589.jpg"
            ],
            "imgAvatar": "a9f64bbf18bb4567952f8ab979f7734a.jpg"
        },
        {
            "id": "163",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "164",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "165",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "166",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "167",
            "level_1": 4,
            "level_2": 3,
            "sku": "8602579435249",
            "name": "Laptop Asus VivoBook S15 S510UQ-BQ483T Core i7-8550U / Win 10 15.6 inch",
            "trademark": "Asus",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "18790000 ",
            "marketPrice": "21990000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU Intel Core i7-8550U 1.8GHz up to 4.0GHz 8MB",
                "RAM 8GB DDR4 2400MHz",
                "Đĩa cứng 1TB HDD 5400rpm, x1 slot SSD M2.SATA",
                "Card đồ họa NVIDIA GeForce GT 940MX 2GB GDDR5 + Intel UHD 620"
            ],
            "imgUrl": [
                "a4f6b0b786bccc28d74177c47e8c4496.jpg",
                "b43c33965c825986b156a99217a43063.jpg",
                "c11586a23fe70a34a89b704a57276b13.jpg"
            ],
            "imgAvatar": "a4f6b0b786bccc28d74177c47e8c4496.jpg"
        },
        {
            "id": "168",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "169",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "170",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "171",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "172",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "173",
            "level_1": 4,
            "level_2": 4,
            "sku": "2031166819760",
            "name": "Laptop HP Pavilion 15-cc107TU 3CH56PA Core i5-8250U/Win 10 (15.6 inch) - Gold - Hàng Chính Hãng",
            "trademark": "HP",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "13969000",
            "marketPrice": "15490000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chip: Intel Core i5-8250U",
                "RAM: 4GB DDR4",
                "Ổ cứng: HDD 1TB 5400rpm",
                "Chipset đồ họa: Intel UHD Graphics 620"
            ],
            "imgUrl": [
                "9efc1c52667061058bcd0ec4f143eda5.jpg",
                "1dae79549eb716f698288ed00dda5b4d.jpg",
                "4794ce9ebcdeb2245f97be9298a90ead.jpg"
            ],
            "imgAvatar": "9efc1c52667061058bcd0ec4f143eda5.jpg"
        },
        {
            "id": "174",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "175",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "176",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "177",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "178",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "179",
            "level_1": 4,
            "level_2": 5,
            "sku": "7525629921541",
            "name": "Laptop Lenovo ThinkPad T470 20HEA03LVA Core i5-7200U/DOS 14 inch - Hàng Chính Hãng",
            "trademark": "Lenovo",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "25899000",
            "marketPrice": "29690000",
            "comment": "9",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "CPU: Intel® Core™ i5-7200U",
                "RAM: 8GB DDR4",
                "HDD: 1TB",
                "Màn hình 14 inch HD"
            ],
            "imgUrl": [
                "234c0223e43ae7cc879e11626dbbd3b9.jpg",
                "d937ef640c1ea139b2c97324cc5e368f.jpg",
                "ef08c567fd88cf2f5098af0e988fe41a.jpg"
            ],
            "imgAvatar": "234c0223e43ae7cc879e11626dbbd3b9.jpg"
        },
        {
            "id": "180",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "181",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "182",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "183",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "184",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "185",
            "level_1": 4,
            "level_2": 6,
            "sku": "8708847626408",
            "name": "Màn Hình BenQ GW2270 21.5 Inch FULL HD",
            "trademark": "BenQ",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1860000",
            "marketPrice": "2250000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Loại sản phẩm: Màn hình máy tính",
                "Kích thước: 21.5 Inches",
                "Độ phân giải: 1920x1080 pixels",
                "Màn hình VA góc nhìn rộng 178 độ"
            ],
            "imgUrl": [
                "gw2270_1.u579.d20161011.t162311.244496.jpg",
                "gw2270_2.u579.d20161011.t112341.814547.jpg",
                "gw2270_3.u579.d20161011.t112341.870559.jpg"
            ],
            "imgAvatar": "gw2270_1.u579.d20161011.t162311.244496.jpg"
        },
        {
            "id": "186",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "187",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "188",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "189",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "190",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "191",
            "level_1": 4,
            "level_2": 7,
            "sku": "1396596550333",
            "name": "Máy Chiếu Sony VPL-CX276",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "33500000 ",
            "marketPrice": "35500000 ",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cường độ sáng: 5200 Lumens",
                "Độ phân giải: 1024 x 768 pixels",
                "Độ tương phản: 3100:1",
                "Tuổi thọ bóng đèn: 5000 giờ"
            ],
            "imgUrl": [
                "cx276-1.u579.d20161128.t170152.728929.jpg",
                "cx276-2.u579.d20161128.t170152.768264.jpg",
                "cx276-3.u579.d20161128.t170152.796621.jpg"
            ],
            "imgAvatar": "cx276-1.u579.d20161128.t170152.728929.jpg"
        },
        {
            "id": "192",
            "level_1": 1,
            "level_2": 1,
            "sku": "6850176240830",
            "name": "Điện Thoại Samsung Galaxy J2 Prime - Hàng Chính Hãng",
            "trademark": "Samsung",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2190000",
            "marketPrice": "2690000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Pin rời",
                "Màn hình: 5 inch, DVGA (540 x 960 pixels)"
            ],
            "imgUrl": [
                "fb3602843929cbda46e127747a0e45bc (1).jpg",
                "j2-primecoffee_3.u504.d20161223.t155452.610356.jpg",
                "j2-primecoffee_9.u504.d20161223.t155508.984240.jpg"
            ],
            "imgAvatar": "fb3602843929cbda46e127747a0e45bc.jpg"
        },
        {
            "id": "193",
            "level_1": 1,
            "level_2": 1,
            "sku": "5806978699391",
            "name": "Điện Thoại Nokia 3 - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": " 2590000",
            "marketPrice": "2999000",
            "comment": "23",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 5 inch (720 x 1280 px)",
                "Camera Trước/Sau: 8MP/8MP"
            ],
            "imgUrl": [
                "1.u2769.d20170608.t173214.542380_2 (1).jpg",
                "1.u2769.d20170608.t192621.835641.jpg",
                "2.u2769.d20170608.t192621.884558.jpg"
            ],
            "imgAvatar": "1.u2769.d20170608.t173214.542380_2.jpg"
        },
        {
            "id": "194",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "195",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "196",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "197",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "198",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "199",
            "level_1": 4,
            "level_2": 8,
            "sku": "2685839667372",
            "name": "Hộp Mực In Sapido 26A (CF226A) Cho Máy In HP M402n, HP M402d, HP M402dn, HP M402dw, HP M426 - Hàng Chính Hãng",
            "trademark": "Sapido",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "1100000",
            "marketPrice": "1300000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Dùng cho máy in HP Pro M402n, M402d, M402dn, M402dw, M426",
                "Số lượng trang in lên đến: 2.300 trang",
                "Cho chất lượng bản in rõ, đẹp",
                "Nhanh khô, tiết kiệm chi phí"
            ],
            "imgUrl": [
                "1.u5552.d20171011.t122727.605981.jpg",
                "2.u5552.d20171011.t122727.645613.jpg",
                "3.u5552.d20171011.t122727.688440.jpg"
            ],
            "imgAvatar": "1.u5552.d20171011.t122727.605981.jpg"
        },
        {
            "id": "200",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "201",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "202",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "203",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "204",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "205",
            "level_1": 4,
            "level_2": 9,
            "sku": "3403566647295",
            "name": "Giấy In Ảnh Media 1 Mặt Bóng A4 200gsm 50 Tờ",
            "trademark": "Media Ink",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Giấy in ảnh 1 mặt",
                "Bề mặt Bóng (Glossy)",
                "Kích thước A4 (21 x 29.7cm)",
                "Định lượng 200gsm"
            ],
            "imgUrl": [
                "1.u5552.d20171025.t145512.232892.jpg",
                "2.u5552.d20171025.t145512.265871.jpg",
                "3.u5552.d20171025.t145512.299038.jpg"
            ],
            "imgAvatar": "1.u5552.d20171025.t145512.232892.jpg"
        },
        {
            "id": "206",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "207",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "208",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "209",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "210",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "211",
            "level_1": 4,
            "level_2": 10,
            "sku": "4127297566876",
            "name": "Bộ Lưu Điện UPS APC BX1100LI-MS 550W - Hàng Chính Hãng",
            "trademark": "APC",
            "commitmentName": "Beteno",
            "star": 5,
            "price": "61000",
            "marketPrice": "66000",
            "comment": "19",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Công suất: 1100VA/ 550W",
                "Nguồn vào: 230VAC",
                "Nguồn ra: 230VAC",
                "Thời gian lưu điện: 100% tải lưu khoảng 2 phút"
            ],
            "imgUrl": [
                "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg",
                "f73af4ec738c974329104a31cc7ebb16.jpg",
                "5fc050b9de6b18166915c3f042e93563.jpg"
            ],
            "imgAvatar": "30d91c36a87f98bbbf3b37ce5b8f11c1.jpg"
        },
        {
            "id": "212",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "213",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "214",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "215",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "216",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "217",
            "level_1": 4,
            "level_2": 11,
            "sku": "5002886692305",
            "name": "Đế Tản Nhiệt Cooler Master X-Slim",
            "trademark": "Cooler Master",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "329000",
            "marketPrice": "420000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Màu sắc: Đen",
                "Chất liệu: nhựa, thoáng, đế nhựa",
                "Số lượng quạt: 1 quạt",
                "Kết nối: USB"
            ],
            "imgUrl": [
                "1_33_2.jpg",
                "2_24_8.jpg",
                "3_17_6.jpg"
            ],
            "imgAvatar": "1_33_2.jpg"
        },
        {
            "id": "218",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "219",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "220",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "221",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "222",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "223",
            "level_1": 4,
            "level_2": 12,
            "sku": "2980596722211",
            "name": "Bộ 2 Thanh RAM PC Patriot VIPER 8GB DDR4 2666mHz RED LED - Hàng Chính Hãng",
            "trademark": "Patriot",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "5200000",
            "marketPrice": "5800000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "2 thanh RAM chuẩn DDR4",
                "8GB mỗi thanh",
                "Bus 2666",
                "2 mặt bên màu đen, đèn LED đỏ"
            ],
            "imgUrl": [
                "fbcc94fef417ff61e61952cc75945b08.jpg",
                "8eaddc110bea820cc5cd408e13608968.jpg",
                "0c304592e743ff1eff74632d4ef6ac68.jpg"
            ],
            "imgAvatar": "fbcc94fef417ff61e61952cc75945b08.jpg"
        },
        {
            "id": "224",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "225",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "226",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "227",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "228",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "229",
            "level_1": 5,
            "level_2": 1,
            "sku": "6006339138852",
            "name": "Máy Ảnh Sony Alpha A7 Mark II + Lens 28-70mm",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "30990000",
            "marketPrice": "44990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Full frame 35mm: 117 điểm"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "230",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "231",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "232",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "233",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "234",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "235",
            "level_1": 5,
            "level_2": 2,
            "sku": "2971131930765",
            "name": "Máy Ảnh Ricoh GR II - Hàng Chính Hãng",
            "trademark": "Ricoh",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến CMOS APS-C 16MP",
                "Ống kính Ricoh GR 28mm f/2.8",
                "Quay video Full HD",
                "Dải nhạy sáng ISO: 100 - 25600"
            ],
            "imgUrl": [
                "1102009.jpg",
                "img_443290_1.jpg",
                "img_443291_1.jpg"
            ],
            "imgAvatar": "1102009.jpg"
        },
        {
            "id": "236",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "237",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "238",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "239",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "240",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "241",
            "level_1": 5,
            "level_2": 3,
            "sku": "2984516627204",
            "name": "Máy Ảnh Selfie Lấy Liền Fujifilm Instax Mini 9 - Smoky White",
            "trademark": "Fujifilm",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "14990000 ",
            "marketPrice": "16990000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế ngộ nghĩnh, đáng yêu",
                "Tích hợp gương Selfie cho các tín đồ mê chụp ảnh",
                "Hình ảnh chân thực, rõ nét",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg",
                "mini9_smokey-white_02_md.u5168.d20170608.t122932.415924.jpg",
                "mini9_smokey-white_04_md.u5168.d20170608.t122932.475648.jpg"
            ],
            "imgAvatar": "mini9_smokey-white_01_md.u5168.d20170608.t122932.377328.jpg"
        },
        {
            "id": "242",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "243",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "244",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "245",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "246",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "247",
            "level_1": 5,
            "level_2": 4,
            "sku": "2984516627204",
            "name": "Canon M10 KIT 15-45mm (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "6590000",
            "marketPrice": "10500000",
            "comment": "78",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến Exmor CMOS 24.3MP Full-frame",
                "Khung ngắm điện tử",
                "Màn hình 3.0 inch",
                "Nhiều chế độ chụp hình được cài đặt sẵn"
            ],
            "imgUrl": [
                "6000712761323.u3059.d20170808.t104320.545104.jpg",
                "1444710130000_img_539669_1.jpg",
                "1444710130000_img_539672.jpg"
            ],
            "imgAvatar": "6000712761323.u3059.d20170808.t104320.545104.jpg"
        },
        {
            "id": "248",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "249",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "250",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "251",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "252",
            "level_1": 5,
            "level_2": 5,
            "sku": "6001778229895",
            "name": "Action Camera Sony HDR-AS50",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "4690000",
            "marketPrice": "5990000",
            "comment": "7",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy quay hành động Full HD 1920x1080 60p/50p",
                "Góc quay 170 độ, ống kính Carl Zeiss Tessar",
                "Chống rung Steady shot",
                "Cảm biến 1/2.3 type “Exmor R” CMOS"
            ],
            "imgUrl": [
                "1452113115000_1211909.jpg",
                "1452113287000_img_572114.jpg",
                "1452113287000_img_572115.jpg"
            ],
            "imgAvatar": "1452113115000_1211909.jpg"
        },
        {
            "id": "253",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "254",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "255",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "256",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "257",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "258",
            "level_1": 5,
            "level_2": 6,
            "sku": "6001778229895",
            "name": "Máy Ảnh Canon 750D + Lens 18-55 IS STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "12490000",
            "marketPrice": "17500000",
            "comment": "58",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: CMOS 24.2MP",
                "Bộ xử lý hình ảnh: DIGIC 6",
                "ISO: ISO 100 - ISO 6400",
                "Hệ thống lấy nét: 19 điểm"
            ],
            "imgUrl": [
                "canon_750 1.u2470.d20160627.t172653.jpg",
                "6007698298751_s_01.d20170802.t175550.535618.jpg",
                "canon_750 2.u2470.d20160627.t172653.jpg"
            ],
            "imgAvatar": "canon_750 1.u2470.d20160627.t172653.jpg"
        },
        {
            "id": "259",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "260",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "261",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "262",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "263",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "264",
            "level_1": 5,
            "level_2": 7,
            "sku": "5000880161841",
            "name": "Lens Canon 50mm f/1.8 STM (Lê Bảo Minh)",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "2500000",
            "marketPrice": "2990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế chắc chắn, tiện dụng",
                "Dành cho Canon 52mm",
                "Dễ dàng tháo lắp và sử dụng",
                "Hạn chế hư hỏng do va đập"
            ],
            "imgUrl": [
                "1431230400000_1143786.jpg",
                "1431230400000_img_493248.jpg",
                "1431230400000_img_493246.jpg"
            ],
            "imgAvatar": "1431230400000_1143786.jpg"
        },
        {
            "id": "265",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "266",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "267",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "268",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "269",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "270",
            "level_1": 5,
            "level_2": 8,
            "sku": "2143598026873",
            "name": "Lens Sony Vario-Tessar T* FE 16-35mm F4 ZA OSS - Hàng Chính Hãng",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 5,
            "price": "24190000",
            "marketPrice": "29990000",
            "comment": "48",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Ống kính zoom góc rộng ZEISS 35mm Full Frame",
                "Khoảng cách lấy nét cực tiểu: 0,28 m",
                "Tỷ lệ phóng đại hình ảnh tối đa (x): 0.19x",
                "Đường kính của kính lọc (mm): 72mm"
            ],
            "imgUrl": [
                "sel 16.35_1.u504.d20160916.t152345.799468.jpg",
                "sel 16.35_2.u504.d20160916.t152345.845624.jpg",
                "sel 16.35_3.u504.d20160916.t152345.901346.jpg"
            ],
            "imgAvatar": "sel 16.35_1.u504.d20160916.t152345.799468.jpg"
        },
        {
            "id": "271",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "272",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "273",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "274",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "275",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "276",
            "level_1": 5,
            "level_2": 9,
            "sku": "6004460623810",
            "name": "Máy Ảnh Canon 6D Body",
            "trademark": "Canon",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "27990000 ",
            "marketPrice": "36000000",
            "comment": "40",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Máy ảnh DSLR Fullframe bán chạy nhất",
                "Cảm biến: CMOS 20.2 MP",
                "ISO: 100 - 25600",
                "Hệ thống lấy nét: 11 điểm AF"
            ],
            "imgUrl": [
                "6008064824345_s_01.d20170802.t175558.671980.jpg",
                "892349.jpg",
                "img_272131.jpg"
            ],
            "imgAvatar": "6008064824345_s_01.d20170802.t175558.671980.jpg"
        },
        {
            "id": "277",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "278",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "279",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "280",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "281",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "282",
            "level_1": 5,
            "level_2": 11,
            "sku": "6004460623810",
            "name": "Máy Ảnh Sony DSC H300 - 20.1 Megapixel, Zoom 35x",
            "trademark": "Sony",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "3400000",
            "marketPrice": "4490000",
            "comment": "74",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Cảm biến: Super HAD CCD 20.1 MP",
                "Ống kính siêu zoom 35x",
                "ISO: 80-3200",
                "Hệ thống lấy nét: Tự động"
            ],
            "imgUrl": [
                "sony-4375-189711-1-zoom.jpg",
                "sony-cyber-shot_dsc-h300-1.jpg",
                "sony-4527-189711-2-zoom.jpg"
            ],
            "imgAvatar": "sony-4375-189711-1-zoom.jpg"
        },
        {
            "id": "283",
            "level_1": 1,
            "level_2": 1,
            "sku": "5806035425765",
            "name": "Bộ Xiaomi Note 4X (16GB/3GB) + Miếng Dán Cường Lực + Ốp Lưng Silicon - Hàng Nhập Khẩu",
            "trademark": "Xiaomi",
            "commitmentName": "Khang Nhung Mobile",
            "star": 4,
            "price": "2899000",
            "marketPrice": "4500000",
            "comment": "4",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mới 100%",
                "Thiết kế: Nguyên khối",
                "Miễn phí giao hàng toàn quốc",
                "Bộ Nhớ: 16GB"
            ],
            "imgUrl": [
                "0.0.u5618.d20170918.t154238.396014 (1).jpg",
                "1.u5618.d20170918.t154238.442176.jpg",
                "2.u5618.d20170918.t154238.468137.jpg"
            ],
            "imgAvatar": "0.u5618.d20170918.t154238.396014.jpg"
        },
        {
            "id": "284",
            "level_1": 1,
            "level_2": 2,
            "sku": "8301963445313",
            "name": "Điện Thoại Itel 2180 - Hàng Chính Hãng",
            "trademark": "Itel",
            "commitmentName": "Tiki Trading",
            "star": 4,
            "price": "232000",
            "marketPrice": "290000",
            "comment": "5",
            "residual": "50",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Thiết kế nhỏ gọn",
                "Đèn Pin tiện dụng",
                "Màn hình TFT 1.77 inch",
                "Bộ đôi camera trước/sau"
            ],
            "imgUrl": [
                "den_1.u2751.d20170323.t164806.576933 (1).jpg",
                "den_2.u2751.d20170323.t164806.622718.jpg",
                "den_3.u2751.d20170323.t164806.661150.jpg"
            ],
            "imgAvatar": "den_1.u2751.d20170323.t164806.576933.jpg"
        },
        {
            "id": "285",
            "level_1": 1,
            "level_2": 2,
            "sku": "8307108706813",
            "name": "Điện Thoại Philips E570 - Hàng Chính Hãng",
            "trademark": "Philips",
            "commitmentName": "Viễn Thịnh",
            "star": 3,
            "price": "890000",
            "marketPrice": "1390000",
            "comment": "5",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Pin rời",
                "Màn hình: 2.8 inch, 320 x 240 pixels"
            ],
            "imgUrl": [
                "1.u2769.d20170613.t150757.49685 (1).jpg",
                "2.u2769.d20170613.t150757.105607.jpg",
                "3.u2769.d20170613.t150757.151362.jpg"
            ],
            "imgAvatar": "1.u2769.d20170613.t150757.49685.jpg"
        },
        {
            "id": "286",
            "level_1": 1,
            "level_2": 2,
            "sku": "8304050784395",
            "name": "Điện Thoại Nokia 105 Single Sim (2017) - Hàng Chính Hãng",
            "trademark": " Nokia",
            "commitmentName": "Tiki Trading",
            "star": 3,
            "price": "890000",
            "marketPrice": "1390000",
            "comment": "15",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Các góc cạnh bo tròn",
                "Cổng sạc Micro USB"
            ],
            "imgUrl": [
                "nokia_105-color-blue.u5395.d20170815.t164008.346743 (1).jpg",
                "nokia_105_blue_1.u5395.d20170816.t101905.501371.jpg",
                "nokia_105_blue_2.u5395.d20170816.t101905.538555.jpg"
            ],
            "imgAvatar": "nokia_105-color-blue.u5395.d20170815.t164008.346743.jpg"
        },
        {
            "id": "287",
            "level_1": 1,
            "level_2": 3,
            "sku": "5809851474139",
            "name": "Điện Thoại iPhone 8 Plus 64GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Khoa Nguyễn",
            "star": 5,
            "price": "20490000",
            "marketPrice": "22990000",
            "comment": "16",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Mã Quốc Tế: LL/ZA/ZP/..",
                "Chính hãng, Nguyên seal, Mới 100%, Chưa Active",
                "CPU: Apple A11 Bionic 6 nhân",
                "RAM: 3GB"
            ],
            "imgUrl": [
                "1.u4939.d20170921.t115008.259587 (2).jpg",
                "2.u4939.d20170921.t115008.307795 (1).jpg",
                "3.u4939.d20170921.t115008.346583.jpg"
            ],
            "imgAvatar": "1.u4939.d20170921.t115008.259587.jpg"
        },
        {
            "id": "288",
            "level_1": 1,
            "level_2": 3,
            "sku": "5800536080485",
            "name": "Điện Thoại iPhone 8 64GB - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Khoa Nguyễn",
            "star": 5,
            "price": "17990000",
            "marketPrice": "19990000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Tính năng: Chống nước, chống bụi, 3D touch",
                "CPU: Apple A11 Bionic 6 nhân",
                "Bộ Nhớ: 64GB",
                "Màn hình: LED-backlit IPS LCD, 4.7 inch HD"
            ],
            "imgUrl": [
                "1.u4939.d20170921.t112647.938342 (1).jpg",
                "2.u4939.d20170921.t112648.168587.jpg",
                "3.u4939.d20170921.t112648.206337.jpg"
            ],
            "imgAvatar": "1.u4939.d20170921.t112647.938342.jpg"
        },
        {
            "id": "289",
            "level_1": 1,
            "level_2": 3,
            "sku": "5804266938559",
            "name": "Điện Thoại iPhone 6 32GB (Vàng Đồng) - Hàng Chính Hãng VN/A",
            "trademark": "Apple",
            "commitmentName": "Văn Huy",
            "star": 5,
            "price": "7790000",
            "marketPrice": "8990000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 4.7 inch, HD (1334 x 750 Pixels)",
                "Camera Trước/Sau: 8MP/ 1.2MP",
                "CPU: Apple A8 2 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u2769.d20170609.t114956.258512 (1).jpg",
                "2.u2769.d20170609.t114956.339661.jpg",
                "3.u2769.d20170609.t114956.416839.jpg"
            ],
            "imgAvatar": "1.u2769.d20170609.t114956.258512.jpg"
        },
        {
            "id": "290",
            "level_1": 1,
            "level_2": 4,
            "sku": "4800337144790",
            "name": "iPad Wifi Cellular 32GB New 2017 - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Khoa Huy",
            "star": 5,
            "price": "10900000",
            "marketPrice": "10999000",
            "comment": "19",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1.5 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "CPU: Apple A9 2 nhân 64-bit",
                "Camera Trước/Sau: 1.2MP/ 8MP"
            ],
            "imgUrl": [
                "1.u2769.d20170623.t192712.56188 (2).jpg",
                "2.u2769.d20170623.t192712.104602.jpg",
                "3.u2769.d20170623.t192712.139039.jpg"
            ],
            "imgAvatar": "1.u2769.d20170623.t192712.56188.jpg"
        },
        {
            "id": "291",
            "level_1": 1,
            "level_2": 4,
            "sku": "4808921114013",
            "name": "iPad Wifi Cellular 32GB New 2017 - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Khoa Điền",
            "star": 5,
            "price": "18490000",
            "marketPrice": "20990000 ",
            "comment": "10",
            "residual": "55",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Thiết kế: Nhôm nguyên khối",
                "Camera Trước/Sau: 7MP/ 12MP"
            ],
            "imgUrl": [
                "1.u2769.d20170622.t151452.947848 (2).jpg",
                "2.u2769.d20170622.t151452.994865.jpg",
                "4069509_tinhte_tren_tay_apple_ipad_pro_10-5_1.u2769.d20170622.t145939.187108.jpg"
            ],
            "imgAvatar": "1.u2769.d20170622.t151452.947848 (1).jpg"
        },
        {
            "id": "292",
            "level_1": 1,
            "level_2": 4,
            "sku": "4802314686279",
            "name": "iPad Pro 10.5 inch 512GB Wifi Cellular - Hàng Chính Hãng",
            "trademark": "Apple",
            "commitmentName": "Viễn Thịnh",
            "star": 5,
            "price": "25900000",
            "marketPrice": "28150000",
            "comment": "10",
            "residual": "45",
            "dateEnd": 2,
            "guarantee": "1 năm",
            "Advantages": [
                "Chính hãng, nguyên seal, mới 100%",
                "Miễn phí giao hàng toàn quốc",
                "Màn hình: 10.5, (2224 x 1668 pixels)",
                "CPU: Apple A10X 6 nhân 64-bit"
            ],
            "imgUrl": [
                "1.u2769.d20170622.t160131.475567 (1).jpg",
                "2.u2769.d20170622.t160131.547295.jpg",
                "4069509_tinhte_tren_tay_apple_ipad_pro_10-5_1.u2769.d20170622.t145939.187108_5_5.jpg"
            ],
            "imgAvatar": "1.u2769.d20170622.t160131.475567.jpg"
        }
    ];

export default productData;
