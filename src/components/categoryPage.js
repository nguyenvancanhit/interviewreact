'use strict';

import React from 'react';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Grid from 'material-ui/Grid';
import DraftsIcon from 'material-ui-icons/Drafts';
import PhoneIphone from 'material-ui-icons/PhoneIphone';
import PersonalVideo from 'material-ui-icons/PersonalVideo';
import { Link } from 'react-router';
import $ from 'jquery'
import menuData from '../data/menu';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Aproduct from './Aproduct';
import productData from '../data/productData';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Hidden from 'material-ui/Hidden';
import StarBorder from 'material-ui-icons/StarBorder';
import Star from 'material-ui-icons/Star';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing.unit * 3,
        backgroundColor: theme.palette.background.paper,
    }
});

export default class categoryPage extends React.Component {

    constructor(props) {
        super(props);
        var level_1 = this.props.params.level_1;
        var level_2 = '';
        if (this.props.params.level_2) {
            level_2 = this.props.params.level_2;
        }
        console.log(this.props.params.level_2);
        var productDatas = [];

        if (level_2) {
            productDatas = productData.filter((product) =>(product.level_1 == level_1 && product.level_2 == level_2));
        } else {
            productDatas = productData.filter((product) =>(product.level_1 == level_1));
        }

        this.state = {
            value: 0,
            showSubMenu: false,
            currentSubMenu: 0,
            isMenuSelected: false,
            isSubMenuSelected: false,
            productDataCurent: productDatas,
            productDatas: productDatas,
            priceEnd: 'max',
            priceStart: 0,
            star: 0,
            arraySelect: []
        };
        $("#subMenuBox").css("display", "none");
        this.handleSort = this.handleSort.bind(this);
        this.resetPage = this.resetPage.bind(this);
        //this.selectPrice = this.selectPrice.bind(this);
    };

    onOver(index) {
        this.setState({
            isMenuSelected: true,
            currentSubMenu: index
        });
        $("#subMenuBox").css("display", "block");
        $("#mainAdsBox").css("display", "none");
        $(".nav-item").eq(index).addClass("nav-item-hover");
    }

    onOut() {
        this.setState({
            isMenuSelected: false
        });
        if (!this.state.isSubMenuSelected) {
            $("#subMenuBox").css("display", "none");
            $("#mainAdsBox").css("display", "block");
            $(".nav-item").eq(this.state.currentSubMenu).removeClass("nav-item-hover");
        }
    }

    onSubMenuOver() {
        this.setState({
            isSubMenuSelected: true
        });
        $("#subMenuBox").css("display", "block");
        $("#mainAdsBox").css("display", "none");
        $(".nav-item").eq(this.state.currentSubMenu).addClass("nav-item-hover");
    }

    onSubMenuOut() {
        this.setState({
            isSubMenuSelected: false
        });
        if (!this.state.isMenuSelected) {
            $("#subMenuBox").css("display", "none");
            $("#mainAdsBox").css("display", "block");
            $(".nav-item").eq(this.state.currentSubMenu).removeClass("nav-item-hover");
        }
    }

    resetPage(level_1, level_2) {

        console.log(level_1);
        var productDatas = [];

        if (level_2) {
            productDatas = productData.filter((product) =>(product.level_1 == level_1 && product.level_2 == level_2));
        } else {
            productDatas = productData.filter((product) =>(product.level_1 == level_1));
        }

        this.setState({
            productDataCurent: productDatas,
            productDatas: productDatas,
            priceEnd: 'max',
            priceStart: 0,
            star: 0,
            arraySelect: [],
            value: 0
        })
    }

    handleSort(value) {
        this.setState({value: value});
        if (value == 0) {
            this.state.productDatas.sort((a, b) => Number(a.id) < Number(b.id) ? -1 : 1);
        }
        if (value == 1) {
            this.state.productDatas.sort((a, b) => Number(a.price) < Number(b.price) ? -1 : 1);
        }
        if (value == 2) {
            this.state.productDatas.sort((a, b) => Number(a.price) > Number(b.price) ? -1 : 1);
        }
        this.setState({productDatas: this.state.productDatas});
    };

    selectPrice(priceStart, priceEnd) {
        this.state.priceStart = priceStart;
        this.state.priceEnd = priceEnd;
        if (priceEnd != 'max') {
            this.state.productDatas = this.state.productDataCurent.filter((product) =>
                (product.price >= priceStart && product.price <= priceEnd && product.star >= this.state.star));
        } else {
            this.state.productDatas = this.state.productDataCurent.filter((product) =>(product.price >= priceStart && product.star >= this.state.star));
        }

        console.log(priceStart, priceEnd);
        this.setState({productDatas: this.state.productDatas, priceEnd: priceEnd, priceStart: priceStart});
        this.handleSort(this.state.value);
    }

    selectStar(star) {
        this.state.star = star;
        if (this.state.priceEnd != 'max') {
            this.state.productDatas = this.state.productDataCurent.filter((product) =>
                (product.price >= this.state.priceStart && product.price <= this.state.priceEnd && product.star >= star));
        } else {
            this.state.productDatas = this.state.productDataCurent.filter((product) =>(product.price >= this.state.priceStart && product.star >= star));
        }
        console.log(star);
        this.setState({star: this.state.star, productDatas: this.state.productDatas});
        this.handleSort(this.state.value);
    }

    render() {
        const { value } = this.state;
        const level = this.props.params.level_1;
        const menu = menuData.filter((menu) => menu.level == level)[0];
        if (!menu || menu.level > 11 || menu.level < 1) {
            return <NotFoundPage/>;
        }
        return (
            <div className="category-page">
                <Card className="main-view">
                    <CardContent className="padding-0">
                        <Grid container alignItems="flex-start" direction="row" justify="flex-start">
                            <Grid item lg={3} className="padding-right-0">

                                <div className="nav-padding main-navigation">
                                    <Grid container className="margin-left-10" alignItems="flex-start"
                                          direction="column" justify="stretch">

                                        {menuData.map((menu, index) =>

                                            <Grid item className="margin-top-10 max-width">
                                                <Link to={'/categoryPage/'+menu.level}>
                                                    <Grid container onMouseOut={this.onOut.bind(this)}
                                                          onClick={this.resetPage.bind(this,menu.level,null)}
                                                          onMouseOver={this.onOver.bind(this, index)}
                                                          className="nav-item" alignItems="flex-start" direction="row"
                                                          justify="flex-start">
                                                        <Grid item className="margin-top-10">
                                                            <img key={index} src={'/icons/'+ menu.icon} alt=""
                                                                 className="menu-icon"/>
                                                        </Grid>
                                                        <Grid item className="margin-top-10">
                                                            <div className="menu-title">{menu.type}</div>
                                                        </Grid>
                                                    </Grid>
                                                </Link>
                                            </Grid>
                                        )}

                                    </Grid>
                                </div>
                                <div className="padding-8">
                                    <div className="branch-care">
                                        ĐÁNH GIÁ
                                    </div>
                                    <List component="nav">


                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectStar.bind(this,5)}>

                                            <div>
                                                <Star className="star"></Star>
                                                <Star className="star"></Star>
                                                <Star className="star"></Star>
                                                <Star className="star"></Star>
                                                <Star className="star"></Star> (Đánh giá 5 sao)
                                            </div>

                                        </ListItem>
                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectStar.bind(this,4)}>

                                            <div>
                                                <Grid container direction="row" justify="flex-start"
                                                      alignItems="center">
                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    (Đánh giá ít nhất 4 sao)
                                                </Grid>
                                            </div>

                                        </ListItem>

                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectStar.bind(this,3)}>

                                            <div>
                                                <Grid container direction="row" justify="flex-start"
                                                      alignItems="center">

                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>

                                                    (Đánh giá ít nhất 3 sao)
                                                </Grid>

                                            </div>
                                        </ListItem>
                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectStar.bind(this,2)}>
                                            <div>
                                                <Grid container direction="row" justify="flex-start"
                                                      alignItems="center">
                                                    <Star className="star"></Star>
                                                    <Star className="star"></Star>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    (Đánh giá ít nhất 2 sao)
                                                </Grid>
                                            </div>
                                        </ListItem>
                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectStar.bind(this,1)}>
                                            <div>
                                                <Grid container direction="row" justify="flex-start"
                                                      alignItems="center">
                                                    <Star className="star"></Star>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    <StarBorder className="star-border"></StarBorder>
                                                    (Đánh giá ít nhất 1 sao)
                                                </Grid>
                                            </div>
                                        </ListItem>
                                    </List>

                                    <div className="branch-care">
                                        CHỌN MỨC GIÁ
                                    </div>
                                    <List component="nav">
                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectPrice.bind(this,0,1000000)}>
                                            <div>
                                                Từ 0 đ - 1.000.000 đ
                                            </div>
                                        </ListItem>

                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectPrice.bind(this,1000000,3000000)}>
                                            <div>
                                                Từ 1.000.000 đ - 3.000.000 đ
                                            </div>
                                        </ListItem>
                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectPrice.bind(this,3000000,10000000)}>
                                            <div>
                                                Từ 3.000.000 đ - 10.000.000 đ
                                            </div>
                                        </ListItem>

                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectPrice.bind(this,10000000,30000000)}>
                                            <div>
                                                Từ 10.000.000 đ - 30.000.000 đ
                                            </div>
                                        </ListItem>

                                        <ListItem className="height-menu-category" button
                                                  onClick={this.selectPrice.bind(this,30000000,'max')}>
                                            <div>
                                                Trên 30.000.000 đ
                                            </div>
                                        </ListItem>
                                    </List>
                                </div>

                            </Grid>
                            <Grid item lg={9} className="wrap-main-box">

                                <div id="mainAdsBox">
                                    <Grid container direction="column">
                                        <Grid item className="padding-bottom-0">
                                            <Grid container direction="row">
                                                <Grid item xs={12} sm={12} md={12} lg={12} className="padding-bottom-0">
                                                    <img width="100%" height="340px"
                                                         src={'/imgs/' +menu.imageCategory}/>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item className="ads-box">
                                            <Grid container direction="row" className="row-imge-2" alignItems="center"
                                                  justify="space-around">
                                                <Grid xs={6} sm={6} md={4} lg={4} item className="padding-0">
                                                    <img width="230px" height="70px" className="cursor"
                                                         src="/imgs/10bac60c5e8b42ec5ce5b354bb6e02df.png"/>
                                                </Grid>
                                                <Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
                                                    <img width="230px" height="70px" className="cursor"
                                                         src="/imgs/f280c0f08d9abaf45d4ad7e3646b3d7c.png"/>
                                                </Grid>
                                                <Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
                                                    <img width="230px" height="70px" className="cursor"
                                                         src="/imgs/a113901c3b2d7fc1f2275d26e0818bb6.png"/>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item>
                                            <AppBar position="static">
                                                <Tabs className="tab-select-produc" value={this.state.value}>
                                                    <Tab onClick={this.handleSort.bind(this, 0)} label="Hàng Mới"/>
                                                    <Tab onClick={this.handleSort.bind(this, 1)} label="Giá Thấp"/>
                                                    <Tab onClick={this.handleSort.bind(this, 2)} label="Giá Cao"/>
                                                </Tabs>
                                            </AppBar>
                                            <TabContainer>
                                                <Grid className="border-right" container alignItems="flex-start"
                                                      direction="row" justify="flex-start">
                                                    {this.state.productDatas.map(product => <Grid item xs={12} sm={6}
                                                                                                  md={6} lg={4}>
                                                        <Aproduct key={product.id} {...product} /> </Grid>)}
                                                </Grid>
                                            </TabContainer>
                                        </Grid>
                                    </Grid>
                                </div>

                                <div id="subMenuBox" onMouseOut={this.onSubMenuOut.bind(this)}
                                     onMouseOver={this.onSubMenuOver.bind(this)}>
                                    <Card className="main-ads" onMouseOut={this.onSubMenuOut.bind(this)}
                                          onMouseOver={this.onSubMenuOver.bind(this)}>
                                        <CardContent className="padding-10">
                                            <Grid container alignItems="stretch" direction="row" justify="flex-start">
                                                {menuData[this.state.currentSubMenu].data.map((subMenu, index) =>
                                                    <Grid item lg={3}>
                                                        <Grid className="submenu-column" container
                                                              alignItems="flex-start" direction="column"
                                                              justify="flex-start">
                                                            <Grid item>
                                                                <span
                                                                    className="header-submenu submenu-text">{subMenu.group}</span>
                                                            </Grid>
                                                            {menuData[this.state.currentSubMenu].data[index].data.map((subMenuItem, index) =>
                                                                <Grid item className="submenu-item">
                                                                    <Link
                                                                        onClick={this.resetPage.bind(this,subMenuItem.level_1,subMenuItem.level_2)}
                                                                        to={'/categoryPage/'+subMenuItem.level_1+'/'+subMenuItem.level_2}>{subMenuItem.name}</Link>
                                                                </Grid>
                                                            )}
                                                        </Grid>
                                                    </Grid>
                                                )}
                                                <Hidden mdDown>
                                                    <Grid item lg={3}>
                                                        <img src={menuData[this.state.currentSubMenu].image}/>
                                                    </Grid>
                                                </Hidden>
                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </div>

                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </div>
        );
    }
}
categoryPage.propTypes = {
    classes: PropTypes.object.isRequired,
};
