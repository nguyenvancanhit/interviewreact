/**
 * Created by VanCanh on 3/9/2018.
 */
/**
 * Created by VanCanh on 3/9/2018.
 */
'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import Search from  'material-ui-icons/Search';
import ShoppingCart from 'material-ui-icons/ShoppingCart';
import Menu from 'material-ui-icons/Menu';

class HeaderCustom extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isDisplayedMenu: false
        };
        this.menuOnClick = this.menuOnClick.bind(this);
    }

    menuOnClick() {
        //var isDisplayedMenu = !this.setState.isDisplayedMenu;
        this.setState({
            isDisplayedMenu: !this.state.isDisplayedMenu
        });
    }
    render() {
        return (
            <div className="header">
                <Grid container alignItems="flex-start" direction="row" justify="flex-start">
                    <Grid item xs={12} sm={5} md={2} lg={2}>
                        <Grid container className="logo" alignItems="flex-start" direction="row" justify="flex-start">
                            <Grid item><Link to="/"><img className="icon-nvc" width="88" height="32" src="/imgs/nvc.PNG"/></Link></Grid>
                            <Grid item><img width="70" height="32" src="/imgs/ngaycuache.png"/></Grid>
                        </Grid>
                    </Grid>
                    <Grid item className="search" xs={12} sm={7} md={5} lg={5}>
                        <Grid container className="logo" alignItems="flex-start" direction="row" justify="flex-start">
                            <Grid xs={8} sm={8} md={8} lg={8} item><input type="text"
                                                                          placeholder="Tìm sản phẩm, danh mục hay thương hiệu mong muốn ..."/></Grid>
                            <Grid xs={4} sm={4} md={4} lg={4} item><Button variant="raised"> <Search></Search> TÌM KIẾM</Button></Grid>
                        </Grid>
                    </Grid>
                    <Grid item className="margin-top-10" xs={12} sm={12} md={5} lg={5}>
                        <Grid container className="logo" alignItems="flex-start" direction="row"
                              justify="space-between">
                            <Grid item>
                                <Grid container className="logo" alignItems="center" direction="row" justify="center">
                                    <Grid item><img className="cursor" width="23" height="22"
                                                    src="/imgs/theodoidonhang.PNG"/></Grid>
                                    <Grid item className="item-header">Theo dõi đơn hàng</Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container className="logo" alignItems="center" direction="row" justify="center">
                                    <Grid item><img className="cursor" width="23" height="22"
                                                    src="/imgs/thongbaocuatui.PNG"/></Grid>
                                    <Grid item className="item-header">Thông báo của tui</Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container className="logo" alignItems="center" direction="row" justify="center">
                                    <Grid item><img className="cursor" width="23" height="22" src="/imgs/dangnhap.PNG"/></Grid>
                                    <Grid item className="item-header">Đăng nhập tài khoản</Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <a className="header-cart">
                                    <ShoppingCart className="shopping-cart"> </ShoppingCart>Giỏ hàng
                                    <span className="cart-count">0</span>
                                </a>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid container classNasme="tab-menu-2-header" alignItems="flex-start" direction="row"
                      justify="flex-start">
                    <Grid item className="margin-top-10" xs={12} sm={12} md={4} lg={4}>
                        <Grid container alignItems="center" direction="row" justify="flex-start"
                        >
                            <Grid item><img src="/imgs/ic_menu_white_36dp_1x.png"/></Grid>
                            <Grid item className="tex-login-account" onClick={this.menuOnClick}>
                                MỤC SẢN PHẨM
                            </Grid>
                        </Grid>
                    </Grid>

                    <Hidden smDown>
                        <Grid item className="margin-top-10" xs={12} sm={12} md={8} lg={8}>
                            <Grid container alignItems="center" direction="row" justify="space-between">
                                <Grid item>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <Grid item>
                                            <img src="/imgs/2h.PNG"/>
                                        </Grid>
                                        <Grid item className="text-menu-2-header">2-tiếng nhận hàng <br/> Hàng chục nghìn sản phẩm</Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <Grid item>
                                            <img src="/imgs/chinhhang.PNG"/>
                                        </Grid>
                                        <Grid item className="text-menu-2-header">Tất cả sản phẩm <br/> 100% chính hãng</Grid>

                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <Grid item>
                                            <img src="/imgs/hoantra.PNG"/>
                                        </Grid>
                                        <Grid item className="text-menu-2-header">30 ngày đổi trả dễ dàng</Grid>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>

                    </Hidden>

                </Grid>
            </div>
        );
    }
}
HeaderCustom.propTypes = {
    classes: PropTypes.object.isRequired
};
export default HeaderCustom;
