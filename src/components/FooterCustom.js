/**
 * Created by VanCanh on 3/9/2018.
 */
'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

export default class FooterCustom extends React.Component {
    render() {
        return (
            <div>
                <Grid container  alignItems="flex-start" direction="row" justify="flex-start">
                    <Grid item className="margin-top-10" xs={12} sm={6} md={3}  lg={3} >
                        <div className="title-footer">HỖ TRỢ KHÁCH HÀNG</div>
                        <div className="hotline-number">Hotline: 1900-6666</div>
                        <div className="element">(1000/phút, 8-21h kể cả thứ 7,CN)</div>
                        <div className="element">các câu hỏi thường gặp</div>
                        <div className="element">Gửi yêu cầu hỗ trợ</div>
                        <div className="element">Hướng dẫn đặt hàng</div>
                        <div className="element">Phương thức vận chuyển</div>
                        <div className="element">Chính sách đổi trả</div>
                        <div className="element">Hướng dẫn mua trả góp</div>
                    </Grid>
                    <Grid item className="margin-top-10" xs={12} sm={6} md={3}  lg={3}>
                        <div className="title-footer">VỀ NVC </div>
                        <div className="element">Giới thiệu NVC.vn</div>
                        <div className="element">Tuyển dụng</div>
                        <div className="element">chính sách bảo mật</div>
                        <div className="element">Chính sách giải quyết khiếu nại</div>
                        <div className="element">điều khoản sử dụng</div>
                        <div className="element">Hội sách online</div>
                        <div className="element">Giới thiệu NVC xu</div>
                        <div className="element">NVC tư vấn</div>
                    </Grid>
                    <Grid item className="margin-top-10" xs={12} sm={6} md={3} lg={3}>
                        <div className="title-footer">PHƯƠNG THỨC THANH TOÁN </div>
                        <div>
                            <img src="/imgs/thanhtoan.PNG"/>
                        </div>
                    </Grid>
                    <Grid item className="margin-top-10" xs={12} sm={6} md={3} lg={3}>
                        <div className="title-footer">KẾT NỐI VỚI CHÚNG TÔI </div>
                        <div>
                            <img src="/imgs/ketnoi.PNG"/>
                        </div>
                        <div className="title-footer margin-top-10">TẢI ỨNG DỤNG TRÊN ĐIỆN THOẠI </div>
                        <div>
                            <img src="/imgs/ungdung.PNG"/>
                        </div>

                    </Grid>
                </Grid>
            </div>
        );
    }
}
