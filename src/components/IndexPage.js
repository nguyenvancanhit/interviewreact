'use strict';

import React from 'react';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Grid from 'material-ui/Grid';
import DraftsIcon from 'material-ui-icons/Drafts';
import PhoneIphone from 'material-ui-icons/PhoneIphone';
import PersonalVideo from 'material-ui-icons/PersonalVideo';
import { Link } from 'react-router';
import $ from 'jquery'
import menuData from '../data/menu';
import Checkbox from 'material-ui/Checkbox';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Aproduct from './Aproduct';
import productData from '../data/productData';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Hidden from 'material-ui/Hidden';

export default class IndexPage extends React.Component {
   
    constructor(props) {
        super(props);
		var productDatas = [];
        for(var i=0; i<6; i++){
			productDatas.push(productData[i]);
		}
        this.state = {
        	showSubMenu: false,
        	currentSubMenu: 0,
        	isMenuSelected: false,
        	isSubMenuSelected: false,
			productDatas: productDatas,
			arraySelect: []
        }
    };

    onOver(index){
    	this.setState({
    		isMenuSelected: true,
    		currentSubMenu: index
    	});
    	$("#subMenuBox").css("display","block");
    	$("#mainAdsBox").css("display","none");
    	$(".nav-item").eq(index).addClass("nav-item-hover");
    }

    onOut(){
    	this.setState({
    		isMenuSelected: false
    	});
    	if(!this.state.isSubMenuSelected){
	    	$("#subMenuBox").css("display", "none");
	    	$("#mainAdsBox").css("display", "block");
	    	$(".nav-item").eq(this.state.currentSubMenu).removeClass("nav-item-hover");    
	    }
    }

    onSubMenuOver(){
    	this.setState({
    		isSubMenuSelected: true
    	});
    	$("#subMenuBox").css("display","block");
    	$("#mainAdsBox").css("display","none");
    	$(".nav-item").eq(this.state.currentSubMenu).addClass("nav-item-hover");
    }
	hideSubMenu(){
		$("#subMenuBox").css("display","none");
	}
    onSubMenuOut(){
    	this.setState({
    		isSubMenuSelected: false
    	});
    	if(!this.state.isMenuSelected){
	    	$("#subMenuBox").css("display","none");
	    	$("#mainAdsBox").css("display","block");
	    	$(".nav-item").eq(this.state.currentSubMenu).removeClass("nav-item-hover");     
	    }    
	}
	handleChange(value) {
		this.state.productDatas = [];
		var position = this.state.arraySelect.indexOf(value);
		if(position != -1){
			this.state.arraySelect.splice(position,1);
			this.setState({
				arraySelect: this.state.arraySelect
			});
		}else {
			this.state.arraySelect.push(value);
			this.setState({
				arraySelect: this.state.arraySelect
			});
		}

		if (this.state.arraySelect.length) {
			for (var i = 0; i < productData.length; i++) {
				if (this.state.arraySelect.indexOf(productData[i].level_1) != -1) {
					this.state.productDatas.push(productData[i]);
				}
			}
		}else{
			for (var i = 0; i < 6; i++) {
					this.state.productDatas.push(productData[i]);
			}
		}
		this.setState({
			productDatas: this.state.productDatas
		});
		console.log(this.state.productDatas);
		console.log(this.state.arraySelect);


	};

  	render() {
	    return (
		<div>
	      <Card className="main-view">
	          <CardContent className="padding-0">
	            <Grid container alignItems="flex-start" direction="row" justify="flex-start">
	              <Grid item lg={3} className="padding-right-0">


			          <div className="nav-padding main-navigation">
			                <Grid container className="margin-left-10" alignItems="flex-start" direction="column" justify="stretch">
			                    

	          			        {menuData.map((menu, index) =>

	          			        	<Grid item className="margin-top-10 max-width" onClick={this.hideSubMenu.bind(this)}>
										<Link to={'/categoryPage/'+menu.level}>
				                        <Grid container onMouseOut={this.onOut.bind(this)} onMouseOver={this.onOver.bind(this, index)} className="nav-item" alignItems="flex-start" direction="row" justify="flex-start">
				                            <Grid item className="margin-top-10">
				                                <img key={index} src={'/icons/'+ menu.icon} alt="" className="menu-icon"/>
				                            </Grid>
				                            <Grid item className="margin-top-10">
				                                <div className="menu-title">{menu.type}</div>
				                            </Grid>
				                        </Grid>
										</Link>
				                    </Grid>

								)}

			                </Grid>
			          </div>

	              </Grid>
	              <Grid item lg={9} className="wrap-main-box">

	            	<div id="mainAdsBox">
	            		<Grid container direction="column">

	            			<Grid item className="padding-bottom-0">
	            				<Grid container direction="row">
	            					<Grid item xs={12} sm={12} md={6} lg={6} className="padding-bottom-0">
	            						<img width="100%" height="340px" src="/imgs/1916d23e95e1f05f68d882847ddb42fc.jpg"/>
	            					</Grid>
									<Grid item xs={12} sm={12} md={6} lg={6} className="margin-top-8 margin-left-16">
	            						<Grid container direction="column">
		            						<Grid item>
		            							<Grid container direction="row" className="padding-0 ">
		            								<Grid xs={6} sm={6} md={6} lg={6} item className="padding-0">
		            									<img width="100%" height="170px" src="/imgs/4075a72f8d4d87af15f1813b403ba4c1.jpg"/>
		            								</Grid>
		            								<Grid xs={6} sm={6} md={6} lg={6} item className="padding-0">
		            									<img width="100%" height="170px" src="/imgs/ec800aaa4719b1ab6026697b9ba9c426.jpg"/>
		            								</Grid>
		            							</Grid>
											</Grid>
											<Grid item className="padding-top-4">
		            							<Grid container direction="row"  className="padding-0 margin-top-10">
		            								<Grid xs={6} sm={6} md={6} lg={6} item className="padding-0">
		            									<img width="100%" height="170px" src="/imgs/1e8957b806f25a1b3013b953eeecb190.jpg"/>
		            								</Grid>
		            								<Grid xs={6} sm={6} md={6} lg={6} item className="padding-0">
		            									<img width="100%" height="170px" src="/imgs/e08bba2f4c878eb035f6e8d53123cdce.jpg"/>
		            								</Grid>
		            							</Grid>
		            						</Grid>
		            					</Grid>
									</Grid>
	            				</Grid>
	            			</Grid>
	            			<Grid item className="ads-box">
	            				<Grid container direction="row" className="padding-right-15">
	            					<Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
	            						<img width="100%" height="173px" src="/imgs/d4597cfbdea0e403c15115ab07893ee6.jpg"/>
			            			</Grid>
	            					<Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
	            						<img width="100%" height="173px" src="/imgs/21e7487aa34ec6456c9c65938c8a476c.jpg"/>
			            			</Grid>
	            					<Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
	            						<img width="100%" height="173px" src="/imgs/058dc743f228da3826133ac264315857.jpg"/>
			            			</Grid>
	            					<Grid xs={6} sm={6} md={3} lg={3} item className="padding-0">
	            						<img width="100%" height="173px" src="/imgs/1e8957b806f25a1b3013b953eeecb190.jpg"/>
			            			</Grid>
			            		</Grid>
	            			</Grid>
	            		</Grid>

	            		   
	            	</div>

	                <div id="subMenuBox" onMouseOut={this.onSubMenuOut.bind(this)} onMouseOver={this.onSubMenuOver.bind(this)}>
	                	<Card className="main-ads" onMouseOut={this.onSubMenuOut.bind(this)} onMouseOver={this.onSubMenuOver.bind(this)}>
	          				<CardContent className="padding-right-50">
	          					<Grid container alignItems="stretch" direction="row" justify="flex-start">
	          						{menuData[this.state.currentSubMenu].data.map((subMenu, index) =>
	          							<Grid item lg={3}>
		          							<Grid className="submenu-column" container alignItems="flex-start" direction="column" justify="flex-start">
		          								<Grid item>
		          									<span className="header-submenu submenu-text">{subMenu.group}</span>
		          								</Grid>
		          								{menuData[this.state.currentSubMenu].data[index].data.map((subMenuItem, index) =>
		          									<Grid item className="submenu-item">
														<Link onClick ={this.onSubMenuOut.bind(this)} to={'/categoryPage/'+subMenuItem.level_1+'/'+subMenuItem.level_2}>{subMenuItem.name}</Link>
			          								</Grid>
		          								)}
		          							</Grid>
		          						</Grid>
	          						)}
								<Hidden mdDown>
	          						<Grid item lg={3}>
	          							<img src={menuData[this.state.currentSubMenu].image} />
	          						</Grid>
								</Hidden>

	          					</Grid>
	          				</CardContent>
	          			</Card>
	                </div>
	                	
	              </Grid>
	            </Grid>
	          </CardContent>
	        </Card>
			<Hidden xsDown>
			<Grid className="padding-40-top-0" container alignItems="flex-start" direction="row" justify="space-between">
				<Grid item>
					<img width="222" height="150" src="/imgs/1d948e3d5a658198dcfd24415bfc4b0a.jpg"/>
				</Grid>
				<Grid item>
					<img width="490" height="150" src="/imgs/50b344a43979c0e99370a636ed1ba563.jpg"/>
				</Grid>
				<Grid item>
					<img width="490" height="150" src="/imgs/bed73f5213b9c3f1c6ab6cb525f7fb6f.jpg"/>
				</Grid>
			</Grid>
			</Hidden>
			<Card className="product-new">
				<CardContent className="padding-0">
					<Grid className="icon-deal" container alignItems="center" direction="row" justify="flex-start">
						<Grid item>
							<img src="/icons/tikideal.PNG" />
						</Grid>
						<Grid item>
							<div className="title-index-page">
								NVC Deal
							</div>
							<div className="text-index-page">
								Cập nhật hàng giờ tất cả những deal giảm giá đặc biệt trên NVC. Hãy bookmark trang này và quay lại thường xuyên để không bỏ lỡ bạn nhé!
							</div>
						</Grid>
					</Grid>
					<div className="show-border"></div>
					<Grid className="border-deal-product-new" container alignItems="flex-start" direction="row" justify="flex-start">
						<Grid className="padding-10"  item xs={12} sm={12} md={3} lg={3}>
							<div className="branch-care">
								NGÀNH HÀNG QUAN TÂM
							</div>
							<div>
								<List component="nav">
									<Divider />
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 1)}
											  value="1"
											  color="primary"
											/>
          								}
												label="Điện Thoại, Máy Tính Bảng"
										/>
									</ListItem>

									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
												onClick={this.handleChange.bind(this, 2)}
											  value="2"
											  color="primary"
											/>
          								}
												label="Tivi & Thiết Bị Nghe Nhìn"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 3)}
											  value="3"
											  color="primary"
											/>
          								}
												label="Thiết Bị Số - Phụ Kiện Số"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 4)}
											  value="4"
											  color="primary"
											/>
          								}
												label="Laptop & Máy Vi Tính"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 5)}
											  value="5"
											  color="primary"
											/>
          								}
												label="Máy Ảnh Máy Quay Phim"
										/>

									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 6)}
											  value="6"
											  color="primary"
											/>
          								}
												label="Điện Gia Dụng, Điện Lạnh"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 7)}
											  value="7"
											  color="primary"
											/>
          								}
												label="Nhà Cửa Đời Sống"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 8)}
											  value="8"
											  color="primary"
											/>
          								}
												label="Hàng Tiêu Dùng - Thực Phẩm"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 9)}
											  value="9"
											  color="primary"
											/>
          								}
												label="Đồ Chơi Mẹ Và Bé"
										/>
									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 10)}
											  value="10"
											  color="primary"
											/>
          								}
												label="Xe Máy - Ôtô - Xe Đạp"
										/>

									</ListItem>
									<ListItem className="height-menu-category" button divider>
										<FormControlLabel
												control={
											<Checkbox
											onClick={this.handleChange.bind(this, 11)}
											  value="11"
											  color="primary"
											/>
          								}
												label="Sách, VPP & Qùa Tặng"
										/>
									</ListItem>
									<Divider light />
								</List>

							</div>
						</Grid>
						<Grid item xs={12} sm={12} md={9} lg={9}>
							<Grid className="border-right" container alignItems="flex-start" direction="row" justify="flex-start">
								{this.state.productDatas.map(product => <Grid item xs={12} sm={6} md={6} lg={4}> <Aproduct key={product.id} {...product} /> </Grid>)}
							</Grid>
						</Grid>
					</Grid>
				</CardContent>
			</Card>

		</div>
	    );
  }
}
