/**
 * Created by VanCanh on 3/11/2018.
 */
'use strict';
import React from 'react';
import { Link } from 'react-router';
import NotFoundPage from './NotFoundPage';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import FavoriteBorder from 'material-ui-icons/FavoriteBorder';
import ShoppingCart from 'material-ui-icons/ShoppingCart';
import Warning from 'material-ui-icons/Warning';
import StarBorder from 'material-ui-icons/StarBorder';
import Aproduct from './Aproduct';
import productData from '../data/productData';


export default class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 1
        };
    };
    render() {
        const id = this.props.params.id;
        console.log(productData);
        const product = productData.filter((product) => product.id === id)[0];
        const level_1 = product.level_1;
        const productRelative = productData.filter((product) =>(product.level_1 == level_1 && product.id != id) );
        if (!product) {
            return <NotFoundPage/>;
        }
console.log(productRelative);
        //const headerStyle = { backgroundImage: `url(/img/${athlete.cover})` };
        return (
            <div>
                <div className="product-detail">
                    <Grid container alignItems="flex-start" direction="row" justify="flex-start">
                        <Grid item xs={12} sm={12} md={5} lg={5}>
                            <div className="product-image">
                                <Grid container alignItems="flex-start" direction="row" justify="space-between">
                                    <Grid item xs={12} sm={3} md={3} lg={3}>
                                        <div className="product-feature-images-vertical">
                                            <Grid container alignItems="flex-start" direction="row"
                                                  justify="flex-start">
                                                { product.imgUrl.map((url, index) =>
                                                    <Grid item xs={4} sm={12} md={12} lg={12}>
                                                        <div className="thumb-item">
                                                         <span className="flx">
                                                         <img key={index}
                                                             src={'/imgs/'+ url} />
                                                         </span>
                                                        </div>
                                                    </Grid>
                                                )}

                                            </Grid>

                                        </div>
                                    </Grid>
                                    <Grid item xs={12} sm={9} md={9} lg={9}>
                                        <div className="product-reivew-images">
                                            <img alt="Product" width="360" height="360"
                                                 src={'/imgs/'+ product.imgAvatar}/>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                        <Grid item className="text-product-detail" xs={12} sm={12} md={7} lg={7}>
                            <Grid container className="border-product-name" alignItems="center" direction="row"
                                  justify="flex-start">
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <div className="product-name">{product.name}</div>
                                    <div><span>Thương hiệu: </span> <span
                                        className="product-brand">{product.trademark}</span> <span
                                        className="product-sku"><span >SKU:</span><span>{product.sku}</span></span>
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid container alignItems="flex-start" direction="row" justify="flex-start">
                                <Grid item xs={12} sm={12} md={12} lg={8}>
                                    <Grid container className="padding-top-10 text-gray" alignItems="center"
                                          direction="row" justify="flex-start">
                                        <Grid item>
                                            <img src="/imgs/deal-hot@2x.png" width="90" height="24"/>
                                        </Grid>
                                        <Grid item>
                                            Giá:
                                        </Grid>
                                        <Grid item className="price">
                                            {product.price} <span className="vnd">đ</span>
                                        </Grid>
                                        <Grid item>
                                            Đã có VAT
                                        </Grid>
                                    </Grid>
                                    <Grid container className="text-gray" alignItems="center" direction="row"
                                          justify="flex-start">
                                        <Grid item>
                                            <span>Tiết kiệm: </span><span
                                            className="discount-percent">{Math.floor(((product.marketPrice - product.price) / product.marketPrice) * 100) }%</span>
                                            <span>({(product.marketPrice - product.price)} đ)</span>
                                        </Grid>
                                    </Grid>
                                    <Grid container className="text-gray" alignItems="center" direction="row"
                                          justify="flex-start">
                                        <Grid item>
                                            <span>Giá thị trường: </span> <span>{product.marketPrice} đ</span>
                                        </Grid>
                                    </Grid>
                                    <Grid container className="text-deal-process" alignItems="center" direction="row"
                                          justify="flex-start">
                                        <Grid item>
                                            Còn lại {product.residual} sản phẩm
                                        </Grid>
                                        <Grid item xs={8} sm={8} md={8} lg={8}>
                                            <span className="deal-process"><span></span></span>
                                        </Grid>

                                    </Grid>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <div className="show-border"></div>
                                    </Grid>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <ul className="salient-features">
                                            {product.Advantages.map((object, i) =>
                                                <li>{object}</li>
                                            )}
                                        </ul>
                                    </Grid>
                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <div className="show-border"></div>
                                    </Grid>
                                    <Grid container alignItems="center" direction="row" justify="center">
                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                            <div className="alert-warning-border">
                                                <Grid container alignItems="center" direction="row"
                                                      justify="flex-start">
                                                    <Grid item>
                                                        <Warning> </Warning>
                                                    </Grid>
                                                    <Grid item>
                                                        Sản phẩm này có giới hạn số lượng khi đặt hàng: <br/>
                                                        Bạn có thể mua tối đa 2 sản phẩm cho mỗi đơn hàng
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" justify="flex-start">
                                        <Grid item>
                                            <div className="product-numbers"> Số lượng:</div>
                                            <div className="amount-reduction-increase">
                                                <div className="input-group">
                            <span className="input-group-btn">
                              <button className="bootstrap-touchspin-down" type="button">-</button>
                            </span>
                                                    <input value={this.state.number} min="1" max="100"
                                                           className="form-control"/>
                            <span className="input-group-btn">
                              <button className="bootstrap-touchspin-up" type="button">+</button>
                            </span>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item className="add-into-cart">
                                            <Button variant="raised" color="secondary"> <ShoppingCart
                                                color="white"></ShoppingCart> Thêm Vào Giỏ Hàng</Button>
                                        </Grid>
                                        <Grid item>
                                            <FavoriteBorder size="48" className="favorite-border"></FavoriteBorder>
                                        </Grid>
                                    </Grid>

                                </Grid>

                                <Grid item xs={12} sm={12} md={12} lg={4}>
                                    <Grid container alignItems="space-around" direction="row" justify="flex-start">
                                        <Grid item xs={12} sm={6} md={6} lg={12}>
                                            <div className="infor-seller-block-wrap">
                                                <Grid container alignItems="flex-start" direction="row"
                                                      justify="flex-start">
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Grid container alignItems="flex-start" direction="row"
                                                              justify="flex-start">
                                                            <Grid item xs={2} sm={2} md={2} lg={2}>
                                                                <img src="/imgs/thuonghieuIcon.PNG"/>
                                                            </Grid>
                                                            <Grid item xs={10} sm={10} md={10} lg={10}>
                                                                <div
                                                                    className="name-seller">{product.commitmentName}</div>
                                                                <div>
                                                                    { product.star < 5 && [...Array(product.star)].map((x, i) =>
                                                                        <StarBorder
                                                                            className="star-border"></StarBorder>)
                                                                    }
                                                                    {
                                                                        product.star < 5 && [...Array(5 - product.star)].map((x, i) =>
                                                                            <StarBorder ></StarBorder>)
                                                                    }
                                                                    {
                                                                        product.star > 4 && [...Array(5)].map((x, i) =>
                                                                            <StarBorder ></StarBorder>)
                                                                    }


                                                                </div>
                                                                <div className="text-small">
                                                                    Cam kết chính hãng 100%
                                                                </div>
                                                                <div>
                                                                    <Button variant="raised" color="primary">Xem
                                                                        Shop</Button>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Grid container alignItems="flex-start" direction="row"
                                                              justify="flex-start">
                                                            <Grid item xs={2} sm={2} md={2} lg={2}>
                                                                <img src="/imgs/baohanhIcon.PNG"/>
                                                            </Grid>
                                                            <Grid item xs={10} sm={10} md={10} lg={10}>
                                                                <div className="name-infor-guarantee">Thông Tin Bảo
                                                                    Hành
                                                                </div>
                                                                <div className="text-small">
                                                                    {product.guarantee}
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Grid>

                                        <Grid item xs={12} sm={6} md={6} lg={12}>
                                            <div className="infor-seller-block-wrap">
                                                <Grid container alignItems="flex-start" direction="row"
                                                      justify="flex-start">
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Grid container alignItems="flex-start" direction="row"
                                                              justify="flex-start">
                                                            <Grid item xs={2} sm={2} md={2} lg={2}>
                                                                <img src="/imgs/phoneIcon.PNG"/>
                                                            </Grid>
                                                            <Grid item xs={10} sm={10} md={10} lg={10}>
                                                                <div className="name-infor-guarantee">Liên Hệ</div>
                                                                <div className="hotline">HOTLINE: 1900 6666</div>
                                                                <div className="small-time">(1.000đ/phút, 8-21h cả T7,
                                                                    CN)
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Grid container alignItems="flex-start" direction="row"
                                                              justify="flex-start">
                                                            <Grid item xs={2} sm={2} md={2} lg={2}>
                                                                <img src="/imgs/emailIcon.PNG"/>
                                                            </Grid>
                                                            <Grid item xs={10} sm={10} md={10} lg={10}>
                                                                <div className="hotline">Email: hotro@nvc.vn</div>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid className="padding-18px" item xs={12} sm={12} md={12} lg={12}>
                                                        <Grid container className="sale-product-question"
                                                              alignItems="flex-start" direction="row"
                                                              justify="flex-start">
                                                            <Grid className="hotline" item xs={7} sm={7} md={7} lg={7}>
                                                                Bạn muốn bán hàng cùng Tiki?
                                                            </Grid>
                                                            <Grid item xs={5} sm={5} md={5} lg={5}>
                                                                <Button variant="raised" color="primary">đăng
                                                                    kí</Button>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Grid>
                                        <Grid className="padding-18px" item xs={12} sm={12} md={12} lg={12}>
                                            <Grid container alignItems="center" direction="row" justify="center">
                                                <Grid item>
                                                    <img width="68" height="20" src="/imgs/facelikeIcon.PNG"/>
                                                </Grid>
                                                <Grid item>
                                                    <img width="50" height="20" src="/imgs/chiaseIcon.PNG"/>
                                                </Grid>
                                            </Grid>
                                            <Grid container alignItems="center" direction="row" justify="center">
                                                <Grid item>
                                                    <img width="128" height="20" src="/imgs/saveFaceIcon.PNG"/>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>

                            </Grid>

                            <Grid container alignItems="center" direction="row" justify="flex-start">
                                <div className="show-border"></div>
                            </Grid>
                            <Grid container className="padding-10" alignItems="flex-start" direction="column"
                                  justify="flex-start">
                                <div className="where-deliver-question">BẠN MUỐN GIAO HÀNG Ở ĐÂU?</div>
                                <div className="choose-province">Chọn tỉnh/thành phố <span><img width="12" height="13"
                                                                                                src="/imgs/edit.PNG"/></span>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>

                <div className="padding-40 product-author-title">SẢN PHẨM THƯỜNG ĐƯỢC XEM CÙNG</div>
                <div className="product-view-also">
                    <Grid container alignItems="flex-start" direction="row" justify="space-around">
                        { productRelative.map(product =>
                            <Grid item>
                                <Aproduct key={product.id} {...product} />
                            </Grid>
                        )}
                    </Grid>
                </div>
            </div>
        );
    }
}
