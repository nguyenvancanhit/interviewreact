'use strict';

import React from 'react';
import { Link } from 'react-router';
import { render } from 'react-dom';
import Button from 'material-ui/Button';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import FooterCustom from './FooterCustom';
import HeaderCustom from './HeaderCustom';
import Card, { CardActions, CardContent } from 'material-ui/Card';

export default class Layout extends React.Component {

  render() {
    return (
      <div className="app-container">
        <header>
            <HeaderCustom></HeaderCustom>
        </header>
        <div className="app-content">{this.props.children}</div>
          <div>
              <Card className="sendMail">
                  <CardContent className="padding-0">
                      <Grid className="border-deal-product-new" container alignItems="center" direction="row"
                            justify="flex-start">
                          <Grid item >
                            <img width="50" src="/icons/send-email.png"/>
                          </Grid>
                          <Grid item >
                                <div className="register">Đăng kí bản tin NVC</div>
                              <div>Đừng bỏ lỡ hàng ngàn sản phẩm và chương trình siêu hấp dẫn</div>
                          </Grid>
                          <Grid item >
                            <input className="input-send-mail" placeholder="Địa chỉ email của ban... "/> <Button variant="raised" color="primary">Đăng kí</Button>
                          </Grid>
                      </Grid>
                  </CardContent>
              </Card>
          </div>
        <footer>
                <FooterCustom></FooterCustom>
        </footer>
      </div>
    );
  }
}

const selectedItemStyle = {
 backgroundColor: 'red'
}
