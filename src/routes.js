'use strict';

import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Layout from './components/Layout';
import IndexPage from './components/IndexPage';
import NotFoundPage from './components/NotFoundPage';
import ProductDetail from './components/ProductDetail';
import CategoryPage from './components/CategoryPage';

const routes = (
  <Route path="/" component={Layout}>
    <IndexRoute component={IndexPage}/>
    <Route path="productDetail/:id" component={ProductDetail}/>
    <Route path="categoryPage/:level_1/:level_2" component={CategoryPage}/>
    <Route path="categoryPage/:level_1" component={CategoryPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

export default routes;
